package sena.utils.tests;

import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.ggp.base.util.game.Game;
import org.ggp.base.util.game.GameRepository;
import org.ggp.base.util.statemachine.Role;
import org.junit.Test;

import sena.base.knowledge.indistinguishability.AccessibilityRelationStructure;
import sena.base.knowledge.verifier.KnowledgeVerifier;

public class TestKVerifier {

	private List<Role> roles = new ArrayList<Role>(3);

	@Test
	public void testSmallScaleMuddy3() {
		Game game = GameRepository.getDefaultRepository().getGame("muddyChildren3");
		List<Role> roles = Role.computeRoles(game.getRules());
		Map<Role, AccessibilityRelationStructure> currentKripkeStructure = new HashMap<Role, AccessibilityRelationStructure>(roles.size());
		for (Role role : roles)
			currentKripkeStructure.put(role, new AccessibilityRelationStructure(2));

		new KnowledgeVerifier(roles);

		AccessibilityRelationStructure a = currentKripkeStructure.get(roles.get(1));
		AccessibilityRelationStructure b = currentKripkeStructure.get(roles.get(2));
		AccessibilityRelationStructure c = currentKripkeStructure.get(roles.get(3));
		a.markUseless(1);
		a.unMarkIndistinguishable(0, 1);

		b.markUseless(0);
		b.markUseless(1);
		b.unMarkIndistinguishable(0, 1);

		c.markUseless(0);
		c.unMarkIndistinguishable(0, 1);

		//[] == []
		assertTrue(KnowledgeVerifier.verifyInconsistency(currentKripkeStructure).equals(new ArrayList<Integer>()));
		List<Integer> inc = new ArrayList<>(2);
		inc.add(0);
		inc.add(1);

		//[0,1] == [0,1]
		assertTrue(KnowledgeVerifier.verifyConsistency(currentKripkeStructure).equals(inc));

		b.markIndistinguishable(0, 1);
		c.markUseless(1);

		//[] == []
		assertTrue(KnowledgeVerifier.verifyInconsistency(currentKripkeStructure).equals(new ArrayList<Integer>()));
		//[0,1] == [0,1]
		assertTrue(KnowledgeVerifier.verifyConsistency(currentKripkeStructure).equals(inc));

		b.unMarkIndistinguishable(0, 1);
		c.markIndistinguishable(0, 1);

		//[] == []
		assertTrue(KnowledgeVerifier.verifyInconsistency(currentKripkeStructure).equals(new ArrayList<Integer>()));
		//[0,1] == [0,1]
		assertTrue(KnowledgeVerifier.verifyConsistency(currentKripkeStructure).equals(inc));
	}

	@Test
	public void testMediumScaleMuddy3() {
		Game game = GameRepository.getDefaultRepository().getGame("muddyChildren3");
		List<Role> roles = Role.computeRoles(game.getRules());
		Map<Role, AccessibilityRelationStructure> currentKripkeStructure = new HashMap<Role, AccessibilityRelationStructure>(roles.size());
		for (Role role : roles)
			currentKripkeStructure.put(role, new AccessibilityRelationStructure(20));

		new KnowledgeVerifier(roles);

		AccessibilityRelationStructure a = currentKripkeStructure.get(roles.get(1));
		AccessibilityRelationStructure b = currentKripkeStructure.get(roles.get(2));
		AccessibilityRelationStructure c = currentKripkeStructure.get(roles.get(3));

		for (int i = 0; i<20; i++) {
			a.markUseless(i);
			b.markUseless(i);
			c.markUseless(i);
		}

		//[] == []
		assertTrue(KnowledgeVerifier.verifyConsistency(currentKripkeStructure).equals(new ArrayList<Integer>()));

		a.markUseful(4); // Now all are consistent
		assertTrue(KnowledgeVerifier.verifyInconsistency(currentKripkeStructure).equals(new ArrayList<Integer>()));
		a.markUseless(4); // Now all are inconsistent again

		for (int i = 0; i<20; i++) {
			for(int j = 0; j < 20; j++) {
				a.unMarkIndistinguishable(i, j);
				b.unMarkIndistinguishable(i, j);
				c.unMarkIndistinguishable(i, j);
			}
		}

		// Still all inconsistent
		assertTrue(KnowledgeVerifier.verifyConsistency(currentKripkeStructure).equals(new ArrayList<Integer>()));

		Set<Integer> consistent = new HashSet<Integer>();
		consistent.add(4);
		a.markUseful(4);

		// [4] == [4]
		assertTrue(KnowledgeVerifier.verifyConsistency(currentKripkeStructure).equals(new ArrayList<Integer>(consistent)));

		b.markIndistinguishable(10, 11);
		c.markIndistinguishable(4, 11);
		consistent.add(10);
		consistent.add(11);

		//[4;10;11] == [4;10;11]
		assertTrue(KnowledgeVerifier.verifyConsistency(currentKripkeStructure).equals(new ArrayList<Integer>(consistent)));
		a.markIndistinguishable(15, 10);
		a.markIndistinguishable(15, 17);
		b.markIndistinguishable(15, 19);
		consistent.add(15);
		consistent.add(17);
		consistent.add(19);

		//[4;10;11;15;17;19] == [4;10;11;15;17;19]
		assertTrue(KnowledgeVerifier.verifyConsistency(currentKripkeStructure).equals(new ArrayList<Integer>(consistent)));
		b.unMarkIndistinguishable(10, 11);

		consistent.remove(10);
		consistent.remove(15);
		consistent.remove(17);
		consistent.remove(19);
		assertTrue(KnowledgeVerifier.verifyConsistency(currentKripkeStructure).equals(new ArrayList<Integer>(consistent)));
	}

	@Test
	public void testLargeScaleMuddy3() {
		Game game = GameRepository.getDefaultRepository().getGame("muddyChildren3");
		List<Role> roles = Role.computeRoles(game.getRules());
		Map<Role, AccessibilityRelationStructure> currentKripkeStructure = new HashMap<Role, AccessibilityRelationStructure>(roles.size());
		for (Role role : roles)
			currentKripkeStructure.put(role, new AccessibilityRelationStructure(200));

		new KnowledgeVerifier(roles);

		AccessibilityRelationStructure a = currentKripkeStructure.get(roles.get(1));
		AccessibilityRelationStructure b = currentKripkeStructure.get(roles.get(2));
		AccessibilityRelationStructure c = currentKripkeStructure.get(roles.get(3));

		for (int i = 0; i < 200; i++) {
			a.markUseless(i);
			b.markUseless(i);
			c.markUseless(i);
		}

		for (int i = 0; i<200; i++) {
			for(int j = 0; j < 200; j++) {
				a.unMarkIndistinguishable(i, j);
				b.unMarkIndistinguishable(i, j);
				c.unMarkIndistinguishable(i, j);
			}
		}

		// Still all inconsistent
		assertTrue(KnowledgeVerifier.verifyConsistency(currentKripkeStructure).equals(new ArrayList<Integer>()));
		Set<Integer> consistent = new HashSet<Integer>();
		a.markUseful(100);
		c.markUseful(50);
		consistent.add(50);
		consistent.add(100);
		assertTrue(KnowledgeVerifier.verifyConsistency(currentKripkeStructure).equals(new ArrayList<Integer>(consistent)));

		b.markIndistinguishable(101, 100);
		consistent.add(101);
		c.markIndistinguishable(50, 101);
		b.markIndistinguishable(150, 101);
		consistent.add(150);
		assertTrue(KnowledgeVerifier.verifyConsistency(currentKripkeStructure).equals(new ArrayList<Integer>(consistent)));
		c.markUseless(50);
		assertTrue(KnowledgeVerifier.verifyConsistency(currentKripkeStructure).equals(new ArrayList<Integer>(consistent)));

		a.markIndistinguishable(170, 150);
		b.markIndistinguishable(172, 170);
		consistent.add(170);
		consistent.add(172);

		a.markIndistinguishable(19, 50);
		consistent.add(19);
		assertTrue(KnowledgeVerifier.verifyConsistency(currentKripkeStructure).equals(new ArrayList<Integer>(consistent)));
		a.markUseless(100);
		assertTrue(KnowledgeVerifier.verifyConsistency(currentKripkeStructure).equals(new ArrayList<Integer>()));

		// this should never happen
		c.markUseful(50);
		assertTrue(KnowledgeVerifier.verifyConsistency(currentKripkeStructure).equals(new ArrayList<Integer>(consistent)));
		a.markUseful(100);
		assertTrue(KnowledgeVerifier.verifyConsistency(currentKripkeStructure).equals(new ArrayList<Integer>(consistent)));
		a.markIndistinguishable(120, 50);
		b.markIndistinguishable(120, 199);
		c.markIndistinguishable(100, 1);
		consistent.add(120);
		consistent.add(199);
		consistent.add(1);

		assertTrue(KnowledgeVerifier.verifyConsistency(currentKripkeStructure).equals(new ArrayList<Integer>(consistent)));

		b.unMarkIndistinguishable(120,199);
		consistent.remove(199);
		assertTrue(KnowledgeVerifier.verifyConsistency(currentKripkeStructure).equals(new ArrayList<Integer>(consistent)));
	}
}
