package sena.utils.visitor;

public abstract class Visitable {

	public abstract void accept(Visitor visitior);
}
