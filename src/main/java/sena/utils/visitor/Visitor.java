package sena.utils.visitor;

import org.ggp.base.util.gdl.grammar.Gdl;
import org.ggp.base.util.gdl.grammar.GdlConstant;
import org.ggp.base.util.gdl.grammar.GdlDistinct;
import org.ggp.base.util.gdl.grammar.GdlFunction;
import org.ggp.base.util.gdl.grammar.GdlLiteral;
import org.ggp.base.util.gdl.grammar.GdlNot;
import org.ggp.base.util.gdl.grammar.GdlOr;
import org.ggp.base.util.gdl.grammar.GdlProposition;
import org.ggp.base.util.gdl.grammar.GdlRelation;
import org.ggp.base.util.gdl.grammar.GdlRule;
import org.ggp.base.util.gdl.grammar.GdlSentence;
import org.ggp.base.util.gdl.grammar.GdlTerm;
import org.ggp.base.util.gdl.grammar.GdlVariable;

public abstract class Visitor {

	public void visit(Gdl gdl) {
        // Do nothing; override in a subclass to do something.
    }
    public void visit(GdlTerm term) {
        // Do nothing; override in a subclass to do something.
    }
    public void visit(GdlConstant constant) {
        // Do nothing; override in a subclass to do something.
    }
    public void visit(GdlVariable variable) {
        // Do nothing; override in a subclass to do something.
    }
    public void visit(GdlFunction function) {
        // Do nothing; override in a subclass to do something.
    }
    public void visit(GdlLiteral literal) {
        // Do nothing; override in a subclass to do something.
    }
    public void visit(GdlSentence sentence) {
        // Do nothing; override in a subclass to do something.
    }
    public void visit(GdlRelation relation) {
        // Do nothing; override in a subclass to do something.
    }
    public void visit(GdlProposition proposition) {
        // Do nothing; override in a subclass to do something.
    }
    public void visit(GdlNot not) {
        // Do nothing; override in a subclass to do something.
    }
    public void visit(GdlDistinct distinct) {
        // Do nothing; override in a subclass to do something.
    }
    public void visit(GdlOr or) {
        // Do nothing; override in a subclass to do something.
    }
    public void visit(GdlRule rule) {
        // Do nothing; override in a subclass to do something.
    }
}

