package sena.utils;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import javax.naming.SizeLimitExceededException;

import org.ggp.base.util.gdl.factory.GdlFactory;
import org.ggp.base.util.gdl.grammar.GdlTerm;
import org.ggp.base.util.statemachine.MachineState;
import org.ggp.base.util.statemachine.Move;
import org.ggp.base.util.symbol.factory.exceptions.SymbolFormatException;

import sena.base.knowledge.model.Model;
import sena.base.util.path.PathUtilities;
import sena.base.util.statemachine.Percept;

public class Dummy {


	public static void main(String[] args) throws SymbolFormatException, SizeLimitExceededException {

		MachineState state = new MachineState();

		List<List<Move>> pathToUpdate = new LinkedList<List<Move>>();

		List<Move> jointMove = new ArrayList<Move>(3);
		GdlTerm term1 = GdlFactory.createTerm("(hello1)");
		GdlTerm term2 = GdlFactory.createTerm("(hello2)");
		GdlTerm term3 = GdlFactory.createTerm("(hello3)");
		Move move1 = new Move(term1);
		Move move2 = new Move(term2);
		Move move3 = new Move(term3);

		jointMove.add(move1);
		jointMove.add(move2);
		jointMove.add(move3);
		pathToUpdate.add(jointMove);

		jointMove = new ArrayList<Move>(3);

		jointMove.add(move1);
		jointMove.add(move2);
		jointMove.add(move3);
		pathToUpdate.add(jointMove);

		jointMove = new ArrayList<Move>(3);

		jointMove.add(move1);
		jointMove.add(move2);
		jointMove.add(move3);
		pathToUpdate.add(jointMove);

		System.out.println(pathToUpdate);

		System.out.println(PathUtilities.movePathSplitter(pathToUpdate, 0, 0));
		System.out.println(PathUtilities.movePathSplitter(pathToUpdate, 0, 1));
		System.out.println(PathUtilities.movePathSplitter(pathToUpdate, 0, 2));

		System.out.println(PathUtilities.movePathSplitter(pathToUpdate, 1, 0));
		System.out.println(PathUtilities.movePathSplitter(pathToUpdate, 1, 1));
		System.out.println(PathUtilities.movePathSplitter(pathToUpdate, 1, 2));

		System.out.println(PathUtilities.movePathSplitter(pathToUpdate, 2, 0));
		System.out.println(PathUtilities.movePathSplitter(pathToUpdate, 2, 1));
		System.out.println(PathUtilities.movePathSplitter(pathToUpdate, 2, 2));

		List<List<List<Percept>>> perceptToUpdate = new ArrayList<List<List<Percept>>>();
		List<List<Percept>> perceptAtTurn = new ArrayList<List<Percept>>(3);
		List<Percept> player1Percept = new ArrayList<>(1);
		List<Percept> player2Percept = new ArrayList<>(1);
		List<Percept> player3Percept = new ArrayList<>(1);

		Percept percept1 = new Percept(term1);
		Percept percept2 = new Percept(term2);
		Percept percept3 = new Percept(term3);

		player1Percept.add(percept1);
		player2Percept.add(percept2);
		player3Percept.add(percept3);
		perceptAtTurn.add(player1Percept);
		perceptAtTurn.add(player2Percept);
		perceptAtTurn.add(player3Percept);

		perceptToUpdate.add(perceptAtTurn);

		perceptAtTurn = new ArrayList<List<Percept>>(3);
		player1Percept = new ArrayList<>(1);
		player2Percept = new ArrayList<>(1);
		player3Percept = new ArrayList<>(1);

		player1Percept.add(percept1);
		player2Percept.add(percept2);
		player3Percept.add(percept3);
		perceptAtTurn.add(player1Percept);
		perceptAtTurn.add(player2Percept);
		perceptAtTurn.add(player3Percept);

		perceptToUpdate.add(perceptAtTurn);

		perceptAtTurn = new ArrayList<List<Percept>>(3);
		player1Percept = new ArrayList<>(1);
		player2Percept = new ArrayList<>(1);
		player3Percept = new ArrayList<>(1);

		player1Percept.add(percept1);
		player2Percept.add(percept2);
		player3Percept.add(percept3);
		perceptAtTurn.add(player1Percept);
		perceptAtTurn.add(player2Percept);
		perceptAtTurn.add(player3Percept);

		perceptToUpdate.add(perceptAtTurn);

		System.out.println(perceptToUpdate);

		System.out.println(PathUtilities.perceptsPathSplitter(perceptToUpdate, 0, 0));
		System.out.println(PathUtilities.perceptsPathSplitter(perceptToUpdate, 0, 1));
		System.out.println(PathUtilities.perceptsPathSplitter(perceptToUpdate, 0, 2));

		System.out.println(PathUtilities.perceptsPathSplitter(perceptToUpdate, 1, 0));
		System.out.println(PathUtilities.perceptsPathSplitter(perceptToUpdate, 1, 1));
		System.out.println(PathUtilities.perceptsPathSplitter(perceptToUpdate, 1, 2));

		System.out.println(PathUtilities.perceptsPathSplitter(perceptToUpdate, 2, 0));
		System.out.println(PathUtilities.perceptsPathSplitter(perceptToUpdate, 2, 1));
		System.out.println(PathUtilities.perceptsPathSplitter(perceptToUpdate, 2, 2));
	}

	public static Model updateModel(Model model, MachineState newState, List<List<Move>> newPath){
		model.setModel(newState);
		model.setModelID(newPath);
		return model;
	}
}
