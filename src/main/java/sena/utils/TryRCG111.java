package sena.utils;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

import org.ggp.base.util.game.Game;
import org.ggp.base.util.game.GameRepository;
import org.ggp.base.util.gdl.factory.GdlFactory;
import org.ggp.base.util.gdl.factory.exceptions.GdlFormatException;
import org.ggp.base.util.gdl.grammar.Gdl;
import org.ggp.base.util.gdl.grammar.GdlRelation;
import org.ggp.base.util.gdl.grammar.GdlSentence;
import org.ggp.base.util.statemachine.MachineState;
import org.ggp.base.util.statemachine.Move;
import org.ggp.base.util.statemachine.Role;
import org.ggp.base.util.statemachine.exceptions.MoveDefinitionException;
import org.ggp.base.util.statemachine.exceptions.TransitionDefinitionException;
import org.ggp.base.util.symbol.factory.exceptions.SymbolFormatException;

import sena.base.knowledge.bag.exception.ModelNotFoundException;
import sena.base.knowledge.rules.processor.KnowledgeRulesProcessor;
import sena.base.util.statemachine.exceptions.PerceptDefinitionException;
import sena.base.util.statemachine.implementation.prover.III_ProverStateMachine;

public class TryRCG111 {

	public static void main(String[] args)
			throws SecurityException, IOException, MoveDefinitionException, TransitionDefinitionException,
			PerceptDefinitionException, GdlFormatException, SymbolFormatException, ModelNotFoundException {

		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		// Game game =
		// GameRepository.getDefaultRepository().getGame("numberGuessingEpistemic32");
		// Game game =
		// GameRepository.getDefaultRepository().getGame("muddyChildren");
		//Game game = GameRepository.getDefaultRepository().getGame("russianCardsGame1_1_1");
		Game game = GameRepository.getDefaultRepository().getGame("russianCardsGame3_3_2_CK");
		List<Role> roles = Role.computeRoles(game.getRules());


		III_ProverStateMachine stateMachine = new III_ProverStateMachine();
		List<Gdl> gameRules = game.getRules();

		stateMachine.initialize(gameRules);

		MachineState currentState = stateMachine.getInitialState();
		Set<GdlSentence> newCache = new HashSet<>();
		List<Move> jointMoves = new LinkedList<>();
		Move m = new Move(GdlFactory.createTerm("(deal 1 2 0)"));
		//jointMoves = stateMachine.getRandomJointMove(currentState, roles.get(0), m);
		jointMoves = stateMachine.getRandomJointMove(currentState);
		currentState = stateMachine.getNextState(currentState, jointMoves);

		System.out.println("Trudy's legal moves: " + stateMachine.getLegalMoves(currentState, roles.get(2)));
		boolean oneTime = false;
		while (!stateMachine.isTerminal(currentState)) {

			System.out.println(currentState);
			if (!oneTime)
				KnowledgeRulesProcessor.getKnowledgeTerms(currentState, roles, gameRules, stateMachine);

			/*
			 * System.out.println("Legal Moves for this state: " +
			 * stateMachine.getLegalJointMoves(currentState));
			 *
			 * String in = br.readLine(); Move move = Move.create(in);
			 * jointMoves.add(move);
			 *
			 * in = br.readLine(); move = Move.create(in); jointMoves.add(move);
			 */
			jointMoves = stateMachine.getRandomJointMove(currentState);
			System.out.println(jointMoves);
			System.out.println(stateMachine.getPercepts(currentState, jointMoves));
			currentState = stateMachine.getNextState(currentState, jointMoves);


			//newCache.add((GdlRelation) GdlFactory.create("(knows trudy (hasBob 1))"));
			//newCache.add((GdlRelation) GdlFactory.create("(knows trudy (hasAlice 1))"));
			//stateMachine.hackCache(newCache);

			System.out.println(stateMachine.askAll((GdlRelation) GdlFactory.create("(bigger_than ?m ?n)"), currentState));
			//System.out.println(stateMachine.askAll((GdlRelation) GdlFactory.create("(trudyKnowsAliceNumber)") , currentState));
			//System.out.println(stateMachine.askAll((GdlRelation) GdlFactory.create("(trudyKnowsBobNumber)"), currentState));
			//System.out.println(stateMachine.askAll((GdlRelation) GdlFactory.create("(trudyAckAliceNumber ?a1)"), currentState));
			//System.out.println(stateMachine.askAll((GdlRelation) GdlFactory.create("(trudyAckBobNumber ?b1)"), currentState));


			System.err.println("Trudy's legal moves: " + stateMachine.getLegalMoves(currentState, roles.get(3)));
			System.out.println("===============================================================================================================");
			System.out.println("===============================================================================================================");
			System.out.println("===============================================================================================================");
		}
		System.exit(0);

	}

}
