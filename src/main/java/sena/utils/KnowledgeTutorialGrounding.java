package sena.utils;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import org.ggp.base.util.game.Game;
import org.ggp.base.util.game.GameRepository;
import org.ggp.base.util.gdl.grammar.Gdl;
import org.ggp.base.util.gdl.grammar.GdlLiteral;
import org.ggp.base.util.gdl.grammar.GdlPool;
import org.ggp.base.util.gdl.grammar.GdlRule;
import org.ggp.base.util.gdl.grammar.GdlSentence;
import org.ggp.base.util.propnet.factory.flattener.PropNetFlattener;
import org.ggp.base.util.prover.aima.AimaProver;
import org.ggp.base.util.statemachine.MachineState;
import org.ggp.base.util.statemachine.Role;
import org.ggp.base.util.statemachine.exceptions.MoveDefinitionException;
import org.ggp.base.util.statemachine.exceptions.TransitionDefinitionException;

import sena.base.util.statemachine.implementation.prover.III_ProverStateMachine;

public class KnowledgeTutorialGrounding {

	public static void main(String[] args) throws SecurityException, IOException, MoveDefinitionException, TransitionDefinitionException {
		// TODO Auto-generated method stub
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		// Recieves the game from here. Needs change
		//Game game = GameRepository.getDefaultRepository().getGame("numberGuessingEpistemic32");
		//Game game =  GameRepository.getDefaultRepository().getGame("muddyChildren3");
		//Game game =  GameRepository.getDefaultRepository().getGame("hatsPuzzle1");
		Game game =  GameRepository.getDefaultRepository().getGame("mastermind");
		List<Role> roles = Role.computeRoles(game.getRules());

		III_ProverStateMachine stateMachine = new III_ProverStateMachine();

		List<Gdl> gameRules = game.getRules();
		stateMachine.initialize(gameRules);
		MachineState currentState = stateMachine.getInitialState();

		List<GdlRule> flatDescription = new PropNetFlattener(gameRules).flatten();// This one blows up
		List<Gdl> groundedGameRules = new ArrayList<>(flatDescription.size());

		System.out.println(flatDescription);

		//Iterator<GdlRule> itGdl = flatDescription.iterator();
		Iterator<Gdl> itGdl = gameRules.iterator();
		Set<GdlSentence> knowsL = new HashSet<GdlSentence>();
		GdlRule toSaveRule = null;
		while (itGdl.hasNext()) {

			Gdl next = itGdl.next();
			groundedGameRules.add(next);

			// System.out.println(next);
			if (next instanceof GdlRule) { // keyword knows only appears in the
											// body of the rules
				List<GdlLiteral> body = ((GdlRule) next).getBody();
				Iterator<GdlLiteral> itBody = body.iterator();
				while (itBody.hasNext()) {
					GdlLiteral bodyN = itBody.next();
					if (bodyN instanceof GdlSentence) {
						if (((GdlSentence) bodyN).getName().equals(GdlPool.KNOWS)) {
							toSaveRule = (GdlRule) next;
							GdlSentence knows = ((GdlSentence) bodyN);

							knowsL.add(knows);
						}
					}
				}
			}
		}
		System.out.println(knowsL);
		System.out.println(toSaveRule);
		System.out.println("Initial State: " + currentState);

		AimaProver newProver = new AimaProver(groundedGameRules);

		currentState = stateMachine.getRandomNextState(currentState);
		System.out.println("Next State: "+ currentState);

		Iterator<GdlSentence> setIt = knowsL.iterator();
		//Set<GdlSentence> results = stateMachine.getKnowledgeInsiderFunction(currentState, toSaveRule.getHead());

		//System.out.println(results);
	}

}
