package sena.utils;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.ggp.base.util.game.Game;
import org.ggp.base.util.game.GameRepository;
import org.ggp.base.util.gdl.factory.exceptions.GdlFormatException;
import org.ggp.base.util.statemachine.Role;
import org.ggp.base.util.statemachine.exceptions.GoalDefinitionException;
import org.ggp.base.util.statemachine.exceptions.MoveDefinitionException;
import org.ggp.base.util.statemachine.exceptions.TransitionDefinitionException;
import org.ggp.base.util.symbol.factory.exceptions.SymbolFormatException;

import sena.base.knowledge.indistinguishability.AccessibilityRelationStructure;
import sena.base.knowledge.verifier.KnowledgeVerifier;
import sena.base.util.statemachine.exceptions.PerceptDefinitionException;

public class SimpleTestKVerifier {

	public static void main(String[] args) throws SecurityException, IOException, MoveDefinitionException,
	TransitionDefinitionException, PerceptDefinitionException, GdlFormatException, SymbolFormatException,
	GoalDefinitionException, InterruptedException {

	Game game = GameRepository.getDefaultRepository().getGame("muddyChildren3");
	List<Role> roles = Role.computeRoles(game.getRules());

	Map<Role, AccessibilityRelationStructure> currentKripkeStructure = new HashMap<Role, AccessibilityRelationStructure>(roles.size());
	for (Role role : roles)
		currentKripkeStructure.put(role, new AccessibilityRelationStructure(2));

	new KnowledgeVerifier(roles);

	System.out.println(roles);

	AccessibilityRelationStructure a = currentKripkeStructure.get(roles.get(1));
	AccessibilityRelationStructure b = currentKripkeStructure.get(roles.get(2));
	AccessibilityRelationStructure c = currentKripkeStructure.get(roles.get(3));
	a.markUseless(1);
	a.unMarkIndistinguishable(0, 1);

	b.markUseless(0);
	b.markUseless(1);
	b.unMarkIndistinguishable(0, 1);

	c.markUseless(0);
	c.unMarkIndistinguishable(0, 1);

	System.out.println(currentKripkeStructure);


	System.out.println(KnowledgeVerifier.verifyConsistency(currentKripkeStructure));

	System.out.println(new ArrayList<Integer> (2));
	System.out.println(KnowledgeVerifier.verifyInconsistency(currentKripkeStructure));
	System.exit(0);


	}
}
