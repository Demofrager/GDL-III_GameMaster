package sena.base.player.gamer.event;


import org.ggp.base.util.gdl.grammar.GdlConstant;
import org.ggp.base.util.observer.Event;

import sena.base.util.match.II_Match;

public final class II_GamerNewMatchEvent extends Event
{

    private final II_Match match;
    private final GdlConstant roleName;

    public II_GamerNewMatchEvent(II_Match match, GdlConstant roleName)
    {
        this.match = match;
        this.roleName = roleName;
    }

    public II_Match getMatch()
    {
        return match;
    }

    public GdlConstant getRoleName()
    {
        return roleName;
    }

}