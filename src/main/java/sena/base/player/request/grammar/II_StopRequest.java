package sena.base.player.request.grammar;

import java.util.ArrayList;
import java.util.List;

import org.ggp.base.player.gamer.event.GamerCompletedMatchEvent;
import org.ggp.base.player.gamer.event.GamerUnrecognizedMatchEvent;
import org.ggp.base.player.gamer.exception.StoppingException;
import org.ggp.base.player.request.grammar.Request;
import org.ggp.base.util.gdl.factory.GdlFactory;
import org.ggp.base.util.gdl.grammar.GdlTerm;
import org.ggp.base.util.logging.GamerLogger;
import org.ggp.base.util.symbol.factory.exceptions.SymbolFormatException;

import sena.base.player.gamer.II_Gamer;

public final class II_StopRequest extends Request {
	private final II_Gamer gamer;
	private final String matchId;
	private final GdlTerm move;
	private final List<GdlTerm> percepts;

	public II_StopRequest(II_Gamer gamer, String matchId, String turn, GdlTerm move, List<GdlTerm> percepts) {
		this.gamer = gamer;
		this.matchId = matchId;
		this.move = move;
		this.percepts = percepts;
	}

	@Override
	public String getMatchId() {
		return matchId;
	}

	@Override
	public String process(long receptionTime) {
		// First, check to ensure that this stop request is for the match
		// we're currently playing. If we're not playing a match, or we're
		// playing a different match, send back "busy".
		if (gamer.getMatch() == null || !gamer.getMatch().getMatchId().equals(matchId)) {
			GamerLogger.logError("GamePlayer", "Got stop message not intended for current game: ignoring.");
			gamer.notifyObservers(new GamerUnrecognizedMatchEvent(matchId));
			return "busy";
		}
		try {

			if (move != null) { // Sending Moves List as: <MyMove, OtherPlayerMove>
				List<GdlTerm> moves = new ArrayList<GdlTerm>(2);
				GdlTerm otherPlayerMove = GdlFactory.createTerm("II_Game_No_OpponentMove");
				moves.add(move);
				moves.add(otherPlayerMove);
				gamer.getMatch().appendMoves(moves);
			}

			if (percepts != null) {// Sending Percepts List as: <MyPercepts, OtherPlayerPercepts>
				List<List<GdlTerm>> perceptList = new ArrayList<List<GdlTerm>>(2);

				// Other player percepts:
				List<GdlTerm> otherPlayerPercepts = new ArrayList<GdlTerm>(1);
				GdlTerm otherPlayerPercept = GdlFactory.createTerm("II_Game_No_OpponentPercepts");
				otherPlayerPercepts.add(otherPlayerPercept);
				perceptList.add(percepts);
				perceptList.add(otherPlayerPercepts);
				gamer.getMatch().appendPercepts(perceptList);
			}

			gamer.getMatch().markCompleted(null);
			gamer.notifyObservers(new GamerCompletedMatchEvent());

			gamer.stop();
		} catch (StoppingException e) {
			GamerLogger.logStackTrace("GamePlayer", e);
		} catch (SymbolFormatException e) {
			GamerLogger.logStackTrace("GamePlayer", e);
		}

		// Once the match has ended, set 'roleName' and 'match'
		// to NULL to indicate that we're ready to begin a new match.
		gamer.setRoleName(null);
		gamer.setMatch(null);

		return "done";
	}

	@Override
	public String toString() {
		return "stop";
	}
}