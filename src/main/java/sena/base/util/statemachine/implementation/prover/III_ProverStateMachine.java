package sena.base.util.statemachine.implementation.prover;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

import org.ggp.base.util.gdl.grammar.Gdl;
import org.ggp.base.util.gdl.grammar.GdlConstant;
import org.ggp.base.util.gdl.grammar.GdlPool;
import org.ggp.base.util.gdl.grammar.GdlRelation;
import org.ggp.base.util.gdl.grammar.GdlSentence;
import org.ggp.base.util.gdl.grammar.GdlTerm;
import org.ggp.base.util.logging.GamerLogger;
import org.ggp.base.util.prover.Prover;
import org.ggp.base.util.prover.aima.AimaProver;
import org.ggp.base.util.statemachine.MachineState;
import org.ggp.base.util.statemachine.Move;
import org.ggp.base.util.statemachine.Role;
import org.ggp.base.util.statemachine.exceptions.GoalDefinitionException;
import org.ggp.base.util.statemachine.exceptions.MoveDefinitionException;
import org.ggp.base.util.statemachine.exceptions.TransitionDefinitionException;

import com.google.common.collect.ImmutableList;

import sena.base.util.statemachine.III_StateMachine;
import sena.base.util.statemachine.Percept;
import sena.base.util.statemachine.exceptions.PerceptDefinitionException;
import sena.base.util.statemachine.implementation.prover.query.II_ProverQueryBuilder;
import sena.base.util.statemachine.implementation.prover.result.II_ProverResultParser;

public class III_ProverStateMachine extends III_StateMachine {


	private MachineState initialState;
	private Prover prover;
	private ImmutableList<Role> roles;


	/**
	 * Initialize must be called before using the StateMachine
	 */
	public III_ProverStateMachine() {

	}

	@Override
	public void initialize(List<Gdl> description) {

		prover = new AimaProver(description);
		roles = ImmutableList.copyOf(Role.computeRoles(description));
		initialState = computeInitialState();
	}

	private MachineState computeInitialState() {

		Set<GdlSentence> results = prover.askAll(II_ProverQueryBuilder.getInitQuery(), new HashSet<GdlSentence>());
		MachineState toReturn = new II_ProverResultParser().toState(results);
		return toReturn;
	}

	@Override
	public int getGoal(MachineState state, Role role) throws GoalDefinitionException {

		Set<GdlSentence> context = II_ProverQueryBuilder.getContext(state);
		Set<GdlSentence> results = prover.askAll(II_ProverQueryBuilder.getGoalQuery(role), context);

		if (results.size() != 1) {
			GamerLogger.logError("StateMachine",
					"Got goal results of size: " + results.size() + " when expecting size one.");

			throw new GoalDefinitionException(state, role);
		}

		try {
			GdlRelation relation = (GdlRelation) results.iterator().next();
			GdlConstant constant = (GdlConstant) relation.get(1);

			int toReturn = Integer.parseInt(constant.toString());

			return toReturn;
		} catch (Exception e) {
			throw new GoalDefinitionException(state, role);
		}
	}

	@Override
	public MachineState getInitialState() {
		return initialState;
	}

	@Override
	public List<Move> getLegalMoves(MachineState state, Role role) throws MoveDefinitionException {

		Set<GdlSentence> context = II_ProverQueryBuilder.getContext(state);
		Set<GdlSentence> results = prover.askAll(II_ProverQueryBuilder.getLegalQuery(role), context);

		if (results.isEmpty()) {
			throw new MoveDefinitionException(state, role);
		}

		List<Move> toReturn = new II_ProverResultParser().toMoves(results);
		return toReturn;
	}

	private List<Percept> getPercepts(MachineState state, Role role, List<Move> moves)
			throws PerceptDefinitionException {

		Set<GdlSentence> context = II_ProverQueryBuilder.getContext(state, getRoles(), moves);
		Set<GdlSentence> results = prover.askAll(II_ProverQueryBuilder.getSeesQuery(role),	context);

		if (results.isEmpty()) {
			return new LinkedList<Percept>();
		}

		List<Percept> toReturn = new II_ProverResultParser().toPercepts(role, results);
		return toReturn;
	}

	@Override
	public List<List<Percept>> getPercepts(MachineState state, List<Move> moves) throws PerceptDefinitionException {

		List<List<Percept>> percepts = new ArrayList<List<Percept>>();
		List<Percept> rolePercepts = new ArrayList<Percept>();
		for (Role role : roles) {
			rolePercepts = getPercepts(state, role, moves);
			percepts.add(rolePercepts);
		}
		return percepts;
	}

	@Override
	public MachineState getNextState(MachineState state, List<Move> moves) throws TransitionDefinitionException {

		Set<GdlSentence> context = II_ProverQueryBuilder.getContext(state, getRoles(), moves);
		Set<GdlSentence> results = prover.askAll(II_ProverQueryBuilder.getNextQuery(), context);

		for (GdlSentence sentence : results) {
			if (!sentence.isGround()) {
				throw new TransitionDefinitionException(state, moves);
			}
		}

		MachineState toReturn = new II_ProverResultParser().toState(results);
		return toReturn;
	}

	@Override
	public List<Role> getRoles() {
		return roles;
	}

	@Override
	public boolean isTerminal(MachineState state) {

		Set<GdlSentence> context = II_ProverQueryBuilder.getContext(state);

		boolean toReturn = prover.prove(II_ProverQueryBuilder.getTerminalQuery(),
				context);

		return toReturn;
	}

	//TODO: Maybe the way we are adding the contents to a state is buggy. Need to try this way
	public MachineState toEspitemicState(MachineState state, Set<GdlSentence> epistemicPart){
	        Set<GdlSentence> trues = new HashSet<GdlSentence>();
	        Set<GdlSentence> knows = new HashSet<GdlSentence>();

	        for (GdlSentence result : state.getContents())
	        {
	            trues.add(GdlPool.getRelation(GdlPool.TRUE, new GdlTerm[] { result.get(0) }));
	        }

	        for (GdlSentence result : epistemicPart)
	        {
	        	//TODO: Maybe use this for knowledge
	        	knows.add(GdlPool.getRelation(GdlPool.KNOWS, new GdlTerm[] { result.get(0) }));

	        }
	        Set<GdlSentence> contents = new HashSet<GdlSentence>(trues);
	        contents.addAll(knows);
	        return new MachineState(contents);
	    }
	@Override
	public boolean proveRelation (GdlSentence question, MachineState state){

		boolean prove = prover.prove(question, state.getContents());
		return prove;
	}

	public Set<GdlSentence> askAll (GdlSentence question, MachineState state){

		Set<GdlSentence> results = prover.askAll(question, state.getContents());
		return results;
	}

	@Override
	public Prover getProver() {
		return prover;
	}
}