package sena.base.util.statemachine.implementation.prover.result;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.ggp.base.util.gdl.grammar.GdlConstant;
import org.ggp.base.util.gdl.grammar.GdlPool;
import org.ggp.base.util.gdl.grammar.GdlSentence;
import org.ggp.base.util.gdl.grammar.GdlTerm;
import org.ggp.base.util.statemachine.MachineState;
import org.ggp.base.util.statemachine.Move;
import org.ggp.base.util.statemachine.Role;

import sena.base.util.statemachine.Percept;

/**
 * The parser that transforms answers of the prover regarding
 * the game description to game elements. Namely moves, percepts,
 * roles and states.
 * This class is an extension of the ProverResultParser from the
 * ggp-base package.
 *
 */
public final class II_ProverResultParser {

    public List<Move> toMoves(Set<GdlSentence> results)
    {
        List<Move> moves = new ArrayList<Move>();
        for (GdlSentence result : results)
        {
            moves.add(new Move(result.get(1)));
        }

        return moves;
    }

    public List<Role> toRoles(List<GdlSentence> results)
    {
        List<Role> roles = new ArrayList<Role>();
        for (GdlSentence result : results)
        {
            GdlConstant name = (GdlConstant) result.get(0);
            roles.add(new Role(name));
        }

        return roles;
    }

    public MachineState toState(Set<GdlSentence> results)
    {
        Set<GdlSentence> trues = new HashSet<GdlSentence>();
        for (GdlSentence result : results)
        {
            trues.add(GdlPool.getRelation(GdlPool.TRUE, new GdlTerm[] { result.get(0) }));
        }
        return new MachineState(trues);
    }

    /**
     * Transforms the set of gdl sentences (sees R P) into
     * percepts (P) and returns them as list.
     * Recall that the Prover returns simply (R, P)
     *
     * @param role - Player that is going to recieve the percept
     * @param results - Percepts rules (sees R P).
     * @return List of Percepts.
     */
	public List<Percept> toPercepts(Role role, Set<GdlSentence> results) {
		List<Percept> percepts = new ArrayList<Percept>();

        for (GdlSentence result : results)
        {
        	percepts.add(new Percept(result.get(1)));
        }

        return percepts;
	}

}