package sena.base.util.statemachine;

import java.io.Serializable;

import org.ggp.base.util.gdl.factory.GdlFactory;
import org.ggp.base.util.gdl.grammar.GdlTerm;
import org.ggp.base.util.symbol.factory.exceptions.SymbolFormatException;

/**
 * A Percept represents a proposition can be visible that by a role.
 * Players can have more than one percept per turn.
 * <p>
 * Note that Percept objects are not intrinsically tied to a role. They
 * only express what is visible.
 *
 */
@SuppressWarnings("serial")
public class Percept implements Serializable
{
    protected final GdlTerm contents;

    public Percept(GdlTerm contents)
    {
        this.contents = contents;
    }

    public static Percept create(String contents) {
        try {
            return new Percept(GdlFactory.createTerm(contents));
        } catch (SymbolFormatException e) {
            throw new IllegalArgumentException("Could not parse as move: " + contents, e);
        }
    }

    @Override
    public boolean equals(Object o)
    {
        if ((o != null) && (o instanceof Percept)) {
        	Percept move = (Percept) o;
            return move.contents.equals(contents);
        }

        return false;
    }

    public GdlTerm getContents()
    {
        return contents;
    }

    @Override
    public int hashCode()
    {
        return contents.hashCode();
    }

    @Override
    public String toString()
    {
        return contents.toString();
    }
}