package sena.base.server.threads;

import org.ggp.base.server.request.RequestBuilder;
import org.ggp.base.util.statemachine.Role;

import sena.base.server.III_GameServer;
import sena.base.util.match.II_Match;

public class III_AbortRequestThread extends III_RequestThread {
	 public III_AbortRequestThread(III_GameServer gameServer, II_Match match, Role role, String host, int port, String playerName)
	    {
	        super(gameServer, role, host, port, playerName, 1000, RequestBuilder.getAbortRequest(match.getMatchId()));
	    }

	    @Override
	    protected void handleResponse(String response) {
	        ;
	    }
}
