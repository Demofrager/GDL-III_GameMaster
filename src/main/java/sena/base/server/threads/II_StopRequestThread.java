package sena.base.server.threads;

import java.util.List;

import org.ggp.base.util.statemachine.Move;
import org.ggp.base.util.statemachine.Role;

import sena.base.server.II_GameServer;
import sena.base.server.request.II_RequestBuilder;
import sena.base.util.match.II_Match;
import sena.base.util.statemachine.Percept;

public class II_StopRequestThread extends II_RequestThread{



    public II_StopRequestThread(II_GameServer gameServer, II_Match match,int turn, Move previousMove, List<Percept> percepts,
			 Role role, String host, int port, String playerName) {

		super(gameServer, role, host, port, playerName, match.getPlayClock() * 1000,
				II_RequestBuilder.getStopRequest(match.getMatchId(), turn, previousMove, percepts, match.getGdlScrambler()));


    }

    @Override
    protected void handleResponse(String response) {
        ;
    }
}
