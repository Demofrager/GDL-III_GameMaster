package sena.base.server;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.naming.SizeLimitExceededException;

import org.ggp.base.server.event.ServerAbortedMatchEvent;
import org.ggp.base.server.event.ServerCompletedMatchEvent;
import org.ggp.base.server.event.ServerConnectionErrorEvent;
import org.ggp.base.server.event.ServerIllegalMoveEvent;
import org.ggp.base.server.event.ServerNewGameStateEvent;
import org.ggp.base.server.event.ServerNewMatchEvent;
import org.ggp.base.server.event.ServerNewMovesEvent;
import org.ggp.base.server.event.ServerTimeEvent;
import org.ggp.base.server.event.ServerTimeoutEvent;
import org.ggp.base.util.gdl.factory.exceptions.GdlFormatException;
import org.ggp.base.util.gdl.grammar.Gdl;
import org.ggp.base.util.gdl.grammar.GdlPool;
import org.ggp.base.util.gdl.grammar.GdlSentence;
import org.ggp.base.util.observer.Event;
import org.ggp.base.util.observer.Observer;
import org.ggp.base.util.observer.Subject;
import org.ggp.base.util.statemachine.MachineState;
import org.ggp.base.util.statemachine.Move;
import org.ggp.base.util.statemachine.Role;
import org.ggp.base.util.statemachine.exceptions.GoalDefinitionException;
import org.ggp.base.util.statemachine.exceptions.MoveDefinitionException;
import org.ggp.base.util.statemachine.exceptions.TransitionDefinitionException;
import org.ggp.base.util.symbol.factory.exceptions.SymbolFormatException;

import sena.base.knowledge.KnowledgeRule;
import sena.base.knowledge.bag.exception.ModelNotFoundException;
import sena.base.knowledge.rules.processor.KnowledgeTermsProcessor;
import sena.base.knowledge.statemachine.KnowledgeStateMachine;
import sena.base.knowledge.statemachine.implementation.DynamicEpistemicStateMachine;
import sena.base.server.event.II_ServerMatchUpdatedEvent;
import sena.base.server.threads.III_AbortRequestThread;
import sena.base.server.threads.III_PlayRequestThread;
import sena.base.server.threads.III_PreviewRequestThread;
import sena.base.server.threads.III_RandomPlayRequestThread;
import sena.base.server.threads.III_StartRequestThread;
import sena.base.server.threads.III_StopRequestThread;
import sena.base.util.match.II_Match;
import sena.base.util.match.II_MatchPublisher;
import sena.base.util.statemachine.III_StateMachine;
import sena.base.util.statemachine.Percept;
import sena.base.util.statemachine.exceptions.PerceptDefinitionException;
import sena.base.util.statemachine.implementation.prover.III_ProverStateMachine;

public final class III_GameServer extends Thread implements Subject {
	private final II_Match match;
	private final III_StateMachine stateMachine;
	private MachineState currentState;

	private final List<String> hosts;
	private final List<Integer> ports;
	private final boolean[] playerGetsUnlimitedTime;
	private final boolean[] playerPlaysRandomly;
	private final List<Observer> observers;
	private List<Move> previousMoves;

	// Added
	private List<List<Percept>> percepts;
	private List<Gdl> gameRules;
	private KnowledgeStateMachine knowledgeMachine;
	private long total;
	private List<Role> roles;

	private Map<Role, String> mostRecentErrors;
	private String saveToFilename;
	private String spectatorServerURL;
	private String spectatorServerKey;
	private boolean forceUsingEntireClock;

	/**
	 * This class is the game server for games in GDL-III.
	 *
	 * @param match - GDL-II match
	 * @param inconsistent - number of models allowed inconsistent
	 * @param nrModels - total number of models
	 * @param hosts - List of hosts (addresses of the players) that are going to play the game.
	 * @param ports - The ports of those hosts (players)
	 *
	 */
	public III_GameServer(II_Match match, int nrModels, int inconsistent, List<String> hosts, List<Integer> ports) {

		this.match = match;

		this.hosts = hosts;
		this.ports = ports;

		playerGetsUnlimitedTime = new boolean[hosts.size()];
		playerPlaysRandomly = new boolean[hosts.size()];
		List<Role> roles = Role.computeRoles(match.getGame().getRules());

		for (int r = 0; r < roles.size(); r++) {
			if (roles.get(r).getName() == GdlPool.RANDOM) {
				playerPlaysRandomly[r] = true;
			}
		}

		// Machine that takes care of the states extended to GDL-III
		stateMachine = new III_ProverStateMachine();

		// Changed this to save the description
		gameRules = match.getGame().getRules();
		stateMachine.initialize(gameRules);
		total = 0l;

		currentState = stateMachine.getInitialState();
		this.roles = stateMachine.getRoles();

		knowledgeMachine = new DynamicEpistemicStateMachine(stateMachine, roles, currentState, nrModels, inconsistent);

		System.out.println( "Initial state is: " + currentState.toString());

		previousMoves = new ArrayList<Move>();
		percepts = new ArrayList<List<Percept>>();
		mostRecentErrors = new HashMap<Role, String>();
		match.appendState(currentState.getContents());

		observers = new ArrayList<Observer>();
		spectatorServerURL = null;
		forceUsingEntireClock = false;



	}

	public void startSavingToFilename(String theFilename) {
		saveToFilename = theFilename;
	}

	public String startPublishingToSpectatorServer(String theURL) {
		spectatorServerURL = theURL;
		return publishWhenNecessary();
	}

	@Override
	public void addObserver(Observer observer) {
		observers.add(observer);
	}

	public List<Integer> getGoals() throws GoalDefinitionException {
		List<Integer> goals = new ArrayList<Integer>();
		for (Role role : stateMachine.getRoles()) {
			goals.add(stateMachine.getGoal(currentState, role));
		}

		return goals;
	}

	public III_StateMachine getStateMachine() {
		return stateMachine;
	}

	@Override
	public void notifyObservers(Event event) {
		for (Observer observer : observers) {
			observer.observe(event);
		}

		// Add error events to mostRecentErrors for recording.
		if (event instanceof ServerIllegalMoveEvent) {
			ServerIllegalMoveEvent sEvent = (ServerIllegalMoveEvent) event;
			mostRecentErrors.put(sEvent.getRole(), "IL " + sEvent.getMove());

		} else if (event instanceof ServerTimeoutEvent) {
			ServerTimeoutEvent sEvent = (ServerTimeoutEvent) event;
			mostRecentErrors.put(sEvent.getRole(), "TO");

		} else if (event instanceof ServerConnectionErrorEvent) {
			ServerConnectionErrorEvent sEvent = (ServerConnectionErrorEvent) event;
			mostRecentErrors.put(sEvent.getRole(), "CE");

		}
	}

	// Should be called after each move, to collect all of the errors
	// caused by players and write them into the match description.
	private void appendErrorsToMatchDescription() {
		List<String> theErrors = new ArrayList<String>();
		for (int i = 0; i < stateMachine.getRoles().size(); i++) {
			Role r = stateMachine.getRoles().get(i);
			if (mostRecentErrors.containsKey(r)) {
				theErrors.add(mostRecentErrors.get(r));
			} else {
				theErrors.add("");
			}
		}
		match.appendErrors(theErrors);
		mostRecentErrors.clear();
	}

	/**
	 * 	It sets up all the target knowledge rules necessary (for the game master) to work.
	 *
	 * @param state - current state of the game
	 * @param description a GDL-III description
	 * @throws GdlFormatException
	 * @throws SymbolFormatException
	 * @throws InterruptedException
	 */
	private void setupTargetKnowledgeTerms() throws GdlFormatException, SymbolFormatException, InterruptedException {

		//Set<KnowledgeRule> targetKnowsRules = KnowledgeRulesProcessor2.getKnowledgeTerms(currentState, roles, gameRules, stateMachine);
		Set<KnowledgeRule> targetKnowsRules = KnowledgeTermsProcessor.getKnowledgeTerms( roles, gameRules);
		System.out.println("Knowledge Targets: " + targetKnowsRules);
		knowledgeMachine.setTargetKnowsRules(targetKnowsRules);
	}

	/** It calculates the knowledge of the next state of the game. According to the joint moves
	 * and precepts that the players made. You can implement any class that takes advantage of this method.
	 * And implement your own logic module.
	 *
	 * @param previousMoves
	 * @param percepts
	 * @return - the set of GDL sentences (knows) that are true after the submission of the moves and precepts
	 * @throws MoveDefinitionException
	 * @throws PerceptDefinitionException
	 * @throws TransitionDefinitionException
	 * @throws ModelNotFoundException
	 * @throws SizeLimitExceededException
	 */
	private void updateNextTurnKnowledge(List<Move> previousMoves, List<List<Percept>> percepts) throws MoveDefinitionException, PerceptDefinitionException, TransitionDefinitionException, ModelNotFoundException, SizeLimitExceededException {
		knowledgeMachine.updateNextTurnKnowledge( previousMoves, percepts);
	}

	private Set<GdlSentence> getTurnKnowledge() throws MoveDefinitionException, PerceptDefinitionException, TransitionDefinitionException, ModelNotFoundException, SizeLimitExceededException {
		return knowledgeMachine.getTurnKnowledge(currentState);
	}

	@Override
	public void run() {
		try {
			// Added for Gdl-II
			int turn = 0;

			// For measurements
			long time1 = 0l;
			long time2 = 0l;

			Set<GdlSentence> epistemicState;

			if (match.getPreviewClock() >= 0) {
				sendPreviewRequests();
			}

			notifyObservers(new ServerNewMatchEvent(stateMachine.getRoles(), currentState));
			notifyObservers(new ServerTimeEvent(match.getStartClock() * 1000));

			// Send the start requests. No need to change. No change in the
			// protocol.
			System.out.println( "Sending start requests");
			sendStartRequests();

			appendErrorsToMatchDescription();

			//Added this here
			System.out.println("Initial physical state: " + currentState);
			setupTargetKnowledgeTerms();
			epistemicState = getTurnKnowledge();
			epistemicState.addAll(currentState.getContents());
			currentState = new MachineState(epistemicState);
			System.out.println("Initial epistemic state " + currentState);

			// The game does not ends until a terminal state is reached.
			while (!stateMachine.isTerminal(currentState)) {

				publishWhenNecessary();
				saveWhenNecessary();
				notifyObservers(new ServerNewGameStateEvent(currentState));
				notifyObservers(new ServerTimeEvent(match.getPlayClock() * 1000));
				notifyObservers(new II_ServerMatchUpdatedEvent(match, spectatorServerKey, saveToFilename));

				// Gives back the players joint moves of the turn (X)
				System.out.println( "Sending play requests for turn: " + turn);
				previousMoves = sendPlayRequests(turn);
				System.out.println( "Players moves: " + previousMoves);

				notifyObservers(new ServerNewMovesEvent(previousMoves));

				// Get the percepts of the current state (X) and applying the
				// moves gotten from the players
				percepts = stateMachine.getPercepts(currentState, previousMoves);
				System.out.println( "Players precepts: " + percepts);

				// Update current state to the next (X+1).
				currentState = stateMachine.getNextState(currentState, previousMoves);
				System.out.println( "Resulting physical state is: " + currentState);

				// Update turn.
				turn++;

				time1 = 0l;
				time2 = 0l;

				time1 = System.currentTimeMillis();

				updateNextTurnKnowledge(previousMoves, percepts);
				epistemicState = getTurnKnowledge();

				epistemicState.addAll(currentState.getContents());
				currentState = new MachineState(epistemicState);
				time2 = System.currentTimeMillis();

				System.out.println( "Resulting epistemic state is: " + currentState);
				System.out.println("Time that is took at turn " + turn + ": " + (time2 - time1) + "ms");
				total += (time2 - time1);
				System.out.println( "Updating turn to: " + turn);

				// Save match contents
				match.appendMoves2(previousMoves);
				match.appendPercepts2(percepts);
				match.appendState(currentState.getContents());
				appendErrorsToMatchDescription();

				if (match.isAborted()) {
					return;
				}
			}
			System.out.println("Game Over!");
			match.markCompleted(stateMachine.getGoals(currentState));

			publishWhenNecessary();
			saveWhenNecessary();
			notifyObservers(new ServerNewGameStateEvent(currentState));
			notifyObservers(new ServerCompletedMatchEvent(getGoals()));
			notifyObservers(new II_ServerMatchUpdatedEvent(match, spectatorServerKey, saveToFilename));

			System.out.println( "Sending stop requests with turn: " + turn);
			sendStopRequests(turn, previousMoves, percepts);

			// Timer
			System.out.println("Printing statistics: ");
			System.out.println("Time spend on calculating the knowledge:");
			System.out.println("	Total: " + total + " ms");
			//System.out.println("	Avg: " + total / turn + " ms/round");
		} catch (InterruptedException ie) {
			if (match.isAborted()) {
				return;
			} else {
				ie.printStackTrace();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void abort() {
		try {
			match.markAborted();
			sendAbortRequests();
			System.out.println( "Match Aborted!");
			saveWhenNecessary();
			publishWhenNecessary();
			notifyObservers(new ServerAbortedMatchEvent());
			notifyObservers(new II_ServerMatchUpdatedEvent(match, spectatorServerKey, saveToFilename));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void saveWhenNecessary() {
		if (saveToFilename == null) {
			return;
		}

		try {
			File file = new File(saveToFilename);
			if (!file.exists()) {
				file.createNewFile();
			}
			FileWriter fw = new FileWriter(file);
			BufferedWriter bw = new BufferedWriter(fw);
			bw.write(match.toJSON().toString());
			bw.close();
			fw.close();
		} catch (IOException ie) {
			ie.printStackTrace();
		}
	}

	private String publishWhenNecessary() {
		if (spectatorServerURL == null) {
			return null;
		}

		int nAttempt = 0;
		while (true) {
			try {
				spectatorServerKey = II_MatchPublisher.publishToSpectatorII_Server(spectatorServerURL, match);
				return spectatorServerKey;
			} catch (IOException e) {
				if (nAttempt > 9) {
					e.printStackTrace();
					return null;
				}
			}
			nAttempt++;
		}
	}

	public String getSpectatorServerKey() {
		return spectatorServerKey;
	}

	private synchronized List<Move> sendPlayRequests(int turn) throws InterruptedException, MoveDefinitionException {
		List<III_PlayRequestThread> threads = new ArrayList<III_PlayRequestThread>(hosts.size());
		for (int i = 0; i < hosts.size(); i++) {
			// alter this for GDL-III
			List<Move> legalMoves = stateMachine.getLegalMoves(currentState, stateMachine.getRoles().get(i));

			if (playerPlaysRandomly[i]) {
				threads.add(new III_RandomPlayRequestThread(match, turn, legalMoves));
			} else {

				if (previousMoves.isEmpty() && percepts.isEmpty()) {

					threads.add(new III_PlayRequestThread(this, match, turn, null, null, legalMoves,
							stateMachine.getRoles().get(i), hosts.get(i), ports.get(i),
							getPlayerNameFromMatchForRequest(i), playerGetsUnlimitedTime[i]));
				}

				else {

					threads.add(new III_PlayRequestThread(this, match, turn, previousMoves.get(i), percepts.get(i),
							legalMoves, stateMachine.getRoles().get(i), hosts.get(i), ports.get(i),
							getPlayerNameFromMatchForRequest(i), playerGetsUnlimitedTime[i]));
				}
			}
		}

		for (III_PlayRequestThread thread : threads) {
			thread.start();
		}

		// From here down everything checked
		if (forceUsingEntireClock) {
			Thread.sleep(match.getPlayClock() * 1000);
		}

		List<Move> moves = new ArrayList<Move>();
		// It is ordered
		for (III_PlayRequestThread thread : threads) {
			thread.join();
			moves.add(thread.getMove());
		}

		return moves;
	}

	private synchronized void sendPreviewRequests() throws InterruptedException {
		List<III_PreviewRequestThread> threads = new ArrayList<III_PreviewRequestThread>(hosts.size());
		for (int i = 0; i < hosts.size(); i++) {
			if (!playerPlaysRandomly[i]) {

				threads.add(new III_PreviewRequestThread(this, match, stateMachine.getRoles().get(i), hosts.get(i),
						ports.get(i), getPlayerNameFromMatchForRequest(i)));
			}
		}
		for (III_PreviewRequestThread thread : threads) {
			thread.start();
		}
		if (forceUsingEntireClock) {
			Thread.sleep(match.getStartClock() * 1000);
		}
		for (III_PreviewRequestThread thread : threads) {
			thread.join();
		}
	}

	private synchronized void sendStartRequests() throws InterruptedException {
		List<III_StartRequestThread> threads = new ArrayList<III_StartRequestThread>(hosts.size());
		for (int i = 0; i < hosts.size(); i++) {
			if (!playerPlaysRandomly[i]) {
				Role role = stateMachine.getRoles().get(i);
				threads.add(new III_StartRequestThread(this, match, role, hosts.get(i), ports.get(i),
						getPlayerNameFromMatchForRequest(i)));
			}
		}
		for (III_StartRequestThread thread : threads) {
			thread.start();
		}
		if (forceUsingEntireClock) {
			Thread.sleep(match.getStartClock() * 1000);
		}
		for (III_StartRequestThread thread : threads) {
			thread.join();
		}
	}

	// Need to change this --- Alter the protocol
	private synchronized void sendStopRequests(int turn, List<Move> previousMoves, List<List<Percept>> percepts)
			throws InterruptedException {
		List<III_StopRequestThread> threads = new ArrayList<III_StopRequestThread>(hosts.size());
		for (int i = 0; i < hosts.size(); i++) {
			if (!playerPlaysRandomly[i]) {
				threads.add(new III_StopRequestThread(this, match, turn, previousMoves.get(i), percepts.get(i),
						stateMachine.getRoles().get(i), hosts.get(i), ports.get(i),
						getPlayerNameFromMatchForRequest(i)));
			}
		}
		for (III_StopRequestThread thread : threads) {
			thread.start();
		}
		for (III_StopRequestThread thread : threads) {
			thread.join();
		}

	}

	private void sendAbortRequests() throws InterruptedException {
		List<III_AbortRequestThread> threads = new ArrayList<III_AbortRequestThread>(hosts.size());
		for (int i = 0; i < hosts.size(); i++) {
			if (!playerPlaysRandomly[i]) {
				threads.add(new III_AbortRequestThread(this, match, stateMachine.getRoles().get(i), hosts.get(i),
						ports.get(i), getPlayerNameFromMatchForRequest(i)));
			}
		}
		for (III_AbortRequestThread thread : threads) {
			thread.start();
		}
		for (III_AbortRequestThread thread : threads) {
			thread.join();
		}
		interrupt();
	}

	public void givePlayerUnlimitedTime(int i) {
		playerGetsUnlimitedTime[i] = true;
	}

	public void makePlayerPlayRandomly(int i) {
		playerPlaysRandomly[i] = true;
	}

	// Why would you want to force the game server to wait for the entire clock?
	// This can be used to rate-limit matches, so that you don't overload
	// supporting
	// services like the repository server, spectator server, players, etc.
	public void setForceUsingEntireClock() {
		forceUsingEntireClock = true;
	}

	public II_Match getMatch() {
		return match;
	}

	private String getPlayerNameFromMatchForRequest(int i) {
		if (match.getPlayerNamesFromHost() != null) {
			return match.getPlayerNamesFromHost().get(i);
		} else {
			return "";
		}
	}
}
