package sena.base.players;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import sena.base.player.II_GamePlayer;
import sena.base.player.gamer.II_Gamer;
import sena.base.util.reflection.II_ProjectSearcher;

/**
 * This is a simple command line app for running players.
 *
 * @author schreib
 */
public final class II_PlayerRunner
{
    public static void main(String[] args) throws IOException, InstantiationException, IllegalAccessException
    {


        if (args.length != 2 || args[0].equals("${arg0}")) {
            System.out.println("II_PlayerRunner [port] [name]");
            System.out.println("example: ant II_PlayerRunner -Darg0=4001 -Darg1=II_RandomGamer");
            return;
        }
        int port = Integer.parseInt(args[0]);
        String name = args[1];
        System.out.println("Starting up preconfigured player on port " + port + " using player class named " + name);
        Class<?> chosenGamerClass = null;
        List<String> availableGamers = new ArrayList<String>();
        for (Class<?> gamerClass : II_ProjectSearcher.GAMERS.getConcreteClasses()) {
            availableGamers.add(gamerClass.getSimpleName());
            if (gamerClass.getSimpleName().equals(name)) {
                chosenGamerClass = gamerClass;
            }
        }
        if (chosenGamerClass == null) {
            System.out.println("Could not find player class with that name. Available choices are: " + Arrays.toString(availableGamers.toArray()));
            return;
        }
        II_Gamer gamer = (II_Gamer) chosenGamerClass.newInstance();
        new II_GamePlayer(port, gamer).start();
    }
}
