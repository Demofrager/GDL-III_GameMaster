package sena.base.knowledge.indistinguishability;

import java.util.LinkedList;
import java.util.List;

/**
 * Specialized matrix to build an indistinguishability matrix for epistemic
 * purposes. This class uses a binary array to save the information if a model
 * is indistinguishable from another. The models are saved by int for their ID.
 *
 * e.g.
 *    | M1 | M2 | M3 | M4 |
 * M1 |  t |  f |  f |  f |
 * M2 |  f |  t |  f |  f |
 * M3 |  f |  f |  t |  f |
 * M4 |  f |  f |  f |  t |
 *
 * @author demofrager
 *
 */
public class AccessibilityRelationStructure {

    private static final int DEFAULT_SIZE = 50;
    private int number_of_models;
    private boolean[][] data;

    public AccessibilityRelationStructure() {
        this(DEFAULT_SIZE);
    }

    public AccessibilityRelationStructure(int number_of_models) {
        this.number_of_models = number_of_models;
        data = new boolean[number_of_models][number_of_models];
        this.initializeMatrix();
    }

    public int getNumberOfModels() {
        return number_of_models;
    }

    /**
     * Initializes the matix. When called in the constructor
     * the matrix is filled with true in all positions.
     * We want them to start with like the identity matrix
     */
    private void initializeMatrix() {
        for (int i = 0; i < data.length; i++) {
        	for (int j = 0; j < data.length; j++) {
        		 data[i][j] = true;
        	}
        }
    }

    /**
     * returns a string with the matrix
     * e.g.
     * t f f f
     * f t f f
     * f f t f
     * f f f t
     */
    @Override
	public String toString() {
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < data.length; i++) {
            for (int j = 0; j < data.length; j++) {
                sb.append(data[i][j]);
                sb.append(" ");
            }
            sb.append("\n");
        }
        return sb.toString();
    }

    /**
     * Marks the matrix in the given positions as true.
     * As well as the transposed position.
     * @param model1_index - int symbolizing the index of the model
     * @param model2_index - int symbolizing the index of the other model
     */
    public void markIndistinguishable(int model1_index, int model2_index) {
        data[model1_index][model2_index] = true;
        data[model2_index][model1_index] = true;
    }

    /**
     * Marks the matrix in the given positions as false.
     * As well as the transposed position.
     * @param model1_index - int symbolizing the index of the model
     * @param model2_index - int symbolizing the index of the other model
     */
    public void unMarkIndistinguishable(int model1_index, int model2_index) {
        data[model1_index][model2_index] = false;
        data[model2_index][model1_index] = false;
    }

    /**
     * Verifies if given two indexes of models, they are
     * indistinguishable
     * @param model1_index - int symbolizing the index of the model
     * @param model2_index - int symbolizing the index of the other model
     * @return boolean if they are or not
     */
    public boolean isIndistinguishable(int model1_index, int model2_index) {
        return data[model1_index][model2_index] == true && data[model2_index][model1_index] == true;
    }

    /**
     * Checks the all the models indexes that are indistinguishable given
     * a model index
     * @param model1_index - int symbolizing the index of the model
     * @return List<Integer> all the models indexes
     */
    public List<Integer> getAllIndistinguishableModelsIds(int model1_index){
        List<Integer> aux = new LinkedList<Integer>();

            for(int j = 0; j < data.length; j++){
            	if(model1_index == j) continue;
                if(isIndistinguishable(model1_index, j)) aux.add(j);
            }
        return aux;
    }

    // TODO: Added

    /**
     *
     * @return
     */
    public List<Integer> getAllUsefulModelsIds(){
        List<Integer> aux = new LinkedList<Integer>();

            for(int j = 0; j < data.length; j++){
                if(getUsefulNess(j)) aux.add(j);
            }
        return aux;
    }

    /**
     * Gets the usefullness of the given model.
     * true - good;
     * false - bad
     *
     * @param model1_index - int symbolizing the index of the model
     */
    public boolean getUsefulNess(int model1_index) {
        return data[model1_index][model1_index];
    }

    /**
     * Marks a model as useless
     * @param model1_index - int symbolizing the index of the model
     */
    public void markUseless(int model1_index) {
        data[model1_index][model1_index] = false;
    }

    /**
     * Marks a model as useful;
     * @param model1_index - int symbolizing the index of the model
     */
    public void markUseful(int model1_index) {
        data[model1_index][model1_index] = true;
    }

}