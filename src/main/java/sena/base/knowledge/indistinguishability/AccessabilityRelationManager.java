package sena.base.knowledge.indistinguishability;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import javax.naming.SizeLimitExceededException;

import org.ggp.base.util.gdl.grammar.GdlPool;
import org.ggp.base.util.statemachine.Move;
import org.ggp.base.util.statemachine.Role;

import sena.base.knowledge.model.Model;
import sena.base.util.path.PathUtilities;
import sena.base.util.statemachine.Percept;
/**
 * Manages the Kripke structures necessary for the implementation of the DEL semantics.
 * @author Filipe Sena
 */
public class AccessabilityRelationManager {

	// It is necessary to save the accessibility relations at every round.
	private List<Map<Role, AccessibilityRelationStructure>> kripkeHistory;

	// We use a current Kripke structure as the most recent round's accessibility relations.
	private Map<Role, AccessibilityRelationStructure> currentKripkeStructure;
	private List<Role> roles;

	public AccessabilityRelationManager(List<Role> roles, int numberOfModels) {
		kripkeHistory = new LinkedList<Map<Role, AccessibilityRelationStructure>>();
		currentKripkeStructure = new HashMap<Role, AccessibilityRelationStructure>(roles.size());
		this.roles = roles;

		for (Role role : roles)
			currentKripkeStructure.put(role, new AccessibilityRelationStructure(numberOfModels));
	}

	/**
	 * Updates the current Kripke structure accordingly to the indistinguishability principle.
	 *
	 * @param moveHistory - list of joint moves that the players made
	 * @param perceptHistory - list of joint precepts that the players received
	 * @param models - list of models to test
	 */
	public void updateCurrentAccessabilityRelation(List<List<Move>> moveHistory,
			List<List<List<Percept>>> perceptHistory, List<Model> models) {

		int roleID = 0;
		for (Role role : roles) {
			if (!role.getName().equals(GdlPool.RANDOM)) {// random is not a player
				roleID = getRoleID(role);
				// break into the role partial path
				updateCurrentAccessabilityRelation(role, PathUtilities.movePathSplitter(moveHistory, roleID),
						PathUtilities.perceptsPathSplitter(perceptHistory, roleID), models);
			}
		}
		saveCurrentStructure();
	}

	private void updateCurrentAccessabilityRelation(Role role, List<Move> hisMoveHistory,
			List<List<Percept>> hisPerceptsHistory, List<Model> models) {
		AccessibilityRelationStructure indMatrix = currentKripkeStructure.get(role);
		Model toCheck;
		List<Move> partialModelPath;
		List<List<Percept>> partialModelPercepts;
		int roleID = getRoleID(role);

		for (int i = 0; i < models.size(); i++) {
			toCheck = models.get(i);
			partialModelPath = PathUtilities.movePathSplitter(toCheck.getModelID(), roleID);
			partialModelPercepts = PathUtilities.perceptsPathSplitter(toCheck.getModelPerceptHistory(), roleID);

			// If player has made the same moves in the model and the actual game path,
			// we mark it as useful - to calculate knowledge
			if (hisMoveHistory.equals(partialModelPath) && hisPerceptsHistory.equals(partialModelPercepts))
				indMatrix.markUseful(i);
			else
				indMatrix.markUseless(i);

			Model toCompare;
			List<Move> partialModelPathToCompare;
			List<List<Percept>> partialModelPerceptsToCompare;
			for (int j = i; j < models.size(); j++) {
				if (j != i) { // Using the matrix to save usefulness (j==i)
					toCompare = models.get(j);
					partialModelPathToCompare = PathUtilities.movePathSplitter(toCompare.getModelID(), roleID);
					partialModelPerceptsToCompare = PathUtilities
							.perceptsPathSplitter(toCompare.getModelPerceptHistory(), roleID);

					// indistinguishability principle: compare if paths are equal
					if (partialModelPath.equals(partialModelPathToCompare)
							&& partialModelPercepts.equals(partialModelPerceptsToCompare))
						indMatrix.markIndistinguishable(i, j);
					else
						indMatrix.unMarkIndistinguishable(i, j);
				}
			}
		}
	}

	/**
	 * Returns the current round kripke structure.
	 * Used to calculate the knowledge of the game.
	 * @return current Kripke structure
	 */
	public Map<Role, AccessibilityRelationStructure> getCurrentStructure() {
		return currentKripkeStructure;
	}

	/**
	 * Returns a specific Kripke structure.
	 * Used to calculate the knowledge of the models during a round in the re-sample
	 * @param round - specific round
	 * @return the desired Kripke structure
	 */
	public Map<Role, AccessibilityRelationStructure> getStructure(int round) {
		return kripkeHistory.get(round);
	}

	/**
	 * Updates a specific Kripke structure accordingly to the indistinguishability principle.
	 * Used after re-sampling of a specific round
	 * @param moveHistory - list of joint moves that the players made
	 * @param perceptHistory - list of joint precepts that the players received
	 * @param round - current round that is being re-sampled
	 * @param models - list of models to test
	 * @throws SizeLimitExceededException
	 */
	public void updateAccessabilityRelation(List<List<Move>> moveHistory,
			List<List<List<Percept>>> perceptHistory, int round, List<Model> models) throws SizeLimitExceededException {

		int roleID = 0;
		for (Role role : roles) {
			if (!role.getName().equals(GdlPool.RANDOM)) {
				roleID = getRoleID(role);
				updateAccessabilityRelation(role, PathUtilities.movePathSplitter(moveHistory, roleID, round),
						PathUtilities.perceptsPathSplitter(perceptHistory, roleID, round), round, models);
			}
		}
	}

	private void updateAccessabilityRelation(Role role, List<Move> movePathSplitted,
			List<List<Percept>> perceptsPathSplitted, int round, List<Model> models) throws SizeLimitExceededException {

		AccessibilityRelationStructure indMatrix = getStructure(round).get(role);
		Model toCheck;
		List<Move> partialModelPath;
		List<List<Percept>> partialModelPercepts;
		int roleID = getRoleID(role);

		for (int i = 0; i < models.size(); i++) {
			toCheck = models.get(i);
			partialModelPath = PathUtilities.movePathSplitter(toCheck.getModelID(), roleID, round);
			partialModelPercepts = PathUtilities.perceptsPathSplitter(toCheck.getModelPerceptHistory(), roleID,round);

			// If player has made the same moves in the model and the actual game path,
			// we mark it as useful - to calculate knowledge
			if (movePathSplitted.equals(partialModelPath) && perceptsPathSplitted.equals(partialModelPercepts))
				indMatrix.markUseful(i);
			else
				indMatrix.markUseless(i);

			Model toCompare;
			List<Move> partialModelPathToCompare;
			List<List<Percept>> partialModelPerceptsToCompare;
			for (int j = i; j < models.size(); j++) {
				if (j != i) { // Using the matrix to save usefulness (j==i)
					toCompare = models.get(j);
					partialModelPathToCompare = PathUtilities.movePathSplitter(toCompare.getModelID(), roleID, round);
					partialModelPerceptsToCompare = PathUtilities.perceptsPathSplitter(toCompare.getModelPerceptHistory(), roleID, round);

					// indistinguishability principle: compare if paths are equal
					if (partialModelPath.equals(partialModelPathToCompare)
							&& partialModelPercepts.equals(partialModelPerceptsToCompare))
						indMatrix.markIndistinguishable(i, j);
					else
						indMatrix.unMarkIndistinguishable(i, j);
				}
			}
		}
	}

	@Override
	public String toString() {
		AccessibilityRelationStructure m;
		StringBuilder sb = new StringBuilder();
		for (Role role : roles) {
			if (!role.getName().equals(GdlPool.RANDOM)) {
				m = currentKripkeStructure.get(role);
				sb.append("For the role: " + role.toString() + "\n");
				sb.append(m.toString() + "\n");
			}
		}

		return sb.toString();
	}

	private void saveCurrentStructure() {
		kripkeHistory.add(new HashMap<Role, AccessibilityRelationStructure>(currentKripkeStructure));
	}

	private int getRoleID(Role role) {
		int roleID = 0;
		for (int i = 0; i < roles.size(); i++) {
			if (roles.get(i).equals(role))
				roleID = i;
		}
		return roleID;
	}
}
