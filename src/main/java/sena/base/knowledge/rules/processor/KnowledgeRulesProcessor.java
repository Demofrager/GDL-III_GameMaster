package sena.base.knowledge.rules.processor;

import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import org.ggp.base.util.gdl.factory.GdlFactory;
import org.ggp.base.util.gdl.factory.exceptions.GdlFormatException;
import org.ggp.base.util.gdl.grammar.Gdl;
import org.ggp.base.util.gdl.grammar.GdlLiteral;
import org.ggp.base.util.gdl.grammar.GdlPool;
import org.ggp.base.util.gdl.grammar.GdlRule;
import org.ggp.base.util.gdl.grammar.GdlSentence;
import org.ggp.base.util.prover.Prover;
import org.ggp.base.util.statemachine.MachineState;
import org.ggp.base.util.statemachine.Role;
import org.ggp.base.util.symbol.factory.exceptions.SymbolFormatException;

import sena.base.knowledge.CommonKnowledge;
import sena.base.knowledge.KnowledgeRule;
import sena.base.knowledge.PersonalKnowledge;
import sena.base.util.statemachine.III_StateMachine;

/**
 * This class processes the gdl description in search of the knows literals It
 * returns an abstract set of rules that represent either
 *
 * @author demofrager
 *
 */
public final class KnowledgeRulesProcessor {

	public static Set<KnowledgeRule> getKnowledgeTerms(MachineState state, List<Role> roles, List<Gdl> description,
			III_StateMachine stateMachine) throws GdlFormatException, SymbolFormatException {
		Iterator<Gdl> itGdl = description.iterator();
		Set<GdlSentence> knowsSet = new HashSet<GdlSentence>();

		while (itGdl.hasNext()) {
			Gdl next = itGdl.next();

			if (next instanceof GdlRule) { // keyword knows only appears in the
											// body of the rules
				List<GdlLiteral> body = ((GdlRule) next).getBody();
				Iterator<GdlLiteral> itBody = body.iterator();

				while (itBody.hasNext()) { // Searching for the right term
					GdlLiteral bodyN = itBody.next();
					if (bodyN instanceof GdlSentence) {
						GdlSentence possibleKnows = ((GdlSentence) bodyN);
						if (possibleKnows.getName().equals(GdlPool.KNOWS)) {
							knowsSet.add(possibleKnows);
						}
					}
				}
			}
		}

		return getKnowledgeTerms(state, roles, knowsSet, stateMachine.getProver());
	}

	private static Set<KnowledgeRule> getKnowledgeTerms(MachineState state, List<Role> roles, Set<GdlSentence> knowsSet,
			Prover prover) throws GdlFormatException, SymbolFormatException {

		Iterator<GdlSentence> knowsIt = knowsSet.iterator();
		GdlSentence knows = null;
		Set<KnowledgeRule> toReturn = new HashSet<KnowledgeRule>();

		while (knowsIt.hasNext()) {
			knows = knowsIt.next();

			if (knows.arity() == 1) { // Common knowledge

				if (!knows.isGround()) { // (knows (hasNumber trudy ?n))
					Set<GdlSentence> groundedRules = getGroundedCommonKnowledgeTerms(state, roles, knows, prover);
					for (GdlSentence groundRule : groundedRules) {
						CommonKnowledge c = new CommonKnowledge(groundRule);
						toReturn.add(c);
					}
				} else {// rules should be not grounded, but if they are... better =)
					CommonKnowledge c = new CommonKnowledge(knows);
					toReturn.add(c);
				}
			} else { // Personal knowledge

				if (!knows.isGround()) { // (knows ?r (num ?n)) or (knows ?c
											// (isMuddy ?c)) or (knows trudy
											// (hasNumber alice ?a))
					Set<GdlSentence> groundedRules = getGroundedPersonalKnowledgeTerms(state, roles, knows, prover);
					for (GdlSentence groundRule : groundedRules) {
						PersonalKnowledge c = new PersonalKnowledge(groundRule);
						toReturn.add(c);
					}
				} else { // rules should be not grounded, but if they are... better =)
					PersonalKnowledge c = new PersonalKnowledge(knows);
					toReturn.add(c);
				}
			}
		}
		//System.out.println("These are the knows objects: " + toReturn);
		return toReturn;
	}

	// Working for Russian Cards Game
	private static Set<GdlSentence> getGroundedCommonKnowledgeTerms(MachineState state, List<Role> roles,
			GdlSentence knows, Prover prover) throws GdlFormatException, SymbolFormatException {

		Set<GdlSentence> toReturn = new HashSet<GdlSentence>();
		GdlSentence term = knows.get(0).toSentence();
		Set<GdlSentence> set = prover.askAll(term, state.getContents());

		for (GdlSentence answer : set) {
			Gdl knowsTermGrounded = GdlFactory.create("( knows " + answer + " )");
			if (knowsTermGrounded instanceof GdlSentence)
				toReturn.add((GdlSentence) knowsTermGrounded);

		}

		return toReturn;
	}

	// Working for Muddy Children and Number Guessing Epistemic and Russian
	// Cards Game
	private static Set<GdlSentence> getGroundedPersonalKnowledgeTerms(MachineState state, List<Role> roles,
			GdlSentence knows, Prover prover) throws GdlFormatException, SymbolFormatException {

		Set<GdlSentence> aux = new HashSet<GdlSentence>();
		Set<GdlSentence> toReturn = new HashSet<GdlSentence>();

		String first = knows.get(0).toString();
		String second = knows.get(1).toString();

		Gdl term0 = knows.get(0);
		GdlSentence term1 = knows.get(1).toSentence();
		Set<GdlSentence> set = prover.askAll(term1, state.getContents());

		boolean sanitize = second.contains(first);

		if (!term0.isGround()) {
			for (Role role : roles) {
				if (role.getName().equals(GdlPool.RANDOM))
					continue; // skip random // Warning not sure if possible

				for (GdlSentence answer : set) {
					Gdl knowsTermGrounded = GdlFactory.create("( knows " + role.toString() + " " + answer + " )");
					if (knowsTermGrounded instanceof GdlSentence)
						aux.add((GdlSentence) knowsTermGrounded);
				}
			}

			if (sanitize) { // remove the ones propagated from
							// before.
							// Only keep where is contained

				for (GdlSentence checkIntegrity : aux) {
					String arg1 = checkIntegrity.get(0).toString();
					String arg2 = checkIntegrity.get(1).toString();
					if (arg2.contains(arg1))
						toReturn.add(checkIntegrity);
				}

			} else
				toReturn = aux;

		} else {
			for (GdlSentence answer : set) {
				Gdl knowsTermGrounded = GdlFactory.create("( knows " + term0 + " " + answer + " )");
				if (knowsTermGrounded instanceof GdlSentence)
					aux.add((GdlSentence) knowsTermGrounded);
			}

			if (sanitize) { // remove the ones propagated from
							// before.
							// Only keep where is contained

				for (GdlSentence checkIntegrity : aux) {
					String arg1 = checkIntegrity.get(0).toString();
					String arg2 = checkIntegrity.get(1).toString();
					if (arg2.contains(arg1))
						toReturn.add(checkIntegrity);
				}

			} else
				toReturn = aux;

		}

		return toReturn;
	}
}
