package sena.base.knowledge.rules.processor.tester;

import java.io.IOException;
import java.util.List;

import org.ggp.base.util.game.Game;
import org.ggp.base.util.game.GameRepository;
import org.ggp.base.util.gdl.factory.exceptions.GdlFormatException;
import org.ggp.base.util.gdl.grammar.Gdl;
import org.ggp.base.util.statemachine.MachineState;
import org.ggp.base.util.statemachine.Role;
import org.ggp.base.util.statemachine.exceptions.MoveDefinitionException;
import org.ggp.base.util.statemachine.exceptions.TransitionDefinitionException;
import org.ggp.base.util.symbol.factory.exceptions.SymbolFormatException;

import sena.base.knowledge.rules.processor.KnowledgeTermsProcessor;
import sena.base.util.statemachine.implementation.prover.III_ProverStateMachine;

public class KnowledgeTermsProcessorTester {

	public static void main(String[] args) throws SecurityException, IOException, MoveDefinitionException,
			TransitionDefinitionException, GdlFormatException, SymbolFormatException, InterruptedException {

		// Recieves the game from here. Needs change
		//Game game = GameRepository.getDefaultRepository().getGame("numberGuessingEpistemic32");
		// Game game = GameRepository.getDefaultRepository().getGame("russianCardsGame");
		//Game game = GameRepository.getDefaultRepository().getGame("muddyChildren3");
		Game game = GameRepository.getDefaultRepository().getGame("hatsPuzzle1");
		//Game game = GameRepository.getDefaultRepository().getGame("hatsPuzzle2");
		//Game game = GameRepository.getDefaultRepository().getGame("russianCardsGame1_1_1_CK");
		//Game game = GameRepository.getDefaultRepository().getGame("russianCardsGame1_1_1_smaller");
		//Game game = GameRepository.getDefaultRepository().getGame("russianCardsGame1_1_1");

		List<Role> roles = Role.computeRoles(game.getRules());

		III_ProverStateMachine stateMachine = new III_ProverStateMachine();

		List<Gdl> gameRules = game.getRules();
		stateMachine.initialize(gameRules);
		MachineState currentState = stateMachine.getInitialState();
		System.out.println("Final: " + KnowledgeTermsProcessor.getKnowledgeTerms(roles, gameRules));
//		do {
//			System.out.println("Final: " + KnowledgeRulesProcessor2.getKnowledgeTerms(currentState, roles, gameRules, stateMachine));
//			List<Move> moves = stateMachine.getRandomJointMove(currentState);
//			currentState = stateMachine.getNextState(currentState, moves);
//		}while(!stateMachine.isTerminal(currentState));

		System.exit(0);
	}

}
