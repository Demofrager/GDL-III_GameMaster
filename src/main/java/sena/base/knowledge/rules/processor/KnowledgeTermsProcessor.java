package sena.base.knowledge.rules.processor;

import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import org.ggp.base.util.gdl.factory.GdlFactory;
import org.ggp.base.util.gdl.factory.exceptions.GdlFormatException;
import org.ggp.base.util.gdl.grammar.Gdl;
import org.ggp.base.util.gdl.grammar.GdlConstant;
import org.ggp.base.util.gdl.grammar.GdlLiteral;
import org.ggp.base.util.gdl.grammar.GdlPool;
import org.ggp.base.util.gdl.grammar.GdlRule;
import org.ggp.base.util.gdl.grammar.GdlSentence;
import org.ggp.base.util.gdl.grammar.GdlTerm;
import org.ggp.base.util.gdl.model.ImmutableSentenceDomainModel;
import org.ggp.base.util.gdl.model.SentenceDomainModelFactory;
import org.ggp.base.util.gdl.model.SentenceForm;
import org.ggp.base.util.gdl.model.SentenceFormDomain;
import org.ggp.base.util.statemachine.Role;
import org.ggp.base.util.symbol.factory.exceptions.SymbolFormatException;

import sena.base.knowledge.CommonKnowledge;
import sena.base.knowledge.KnowledgeRule;
import sena.base.knowledge.PersonalKnowledge;

/**
 * This class processes the GDL description in search of the knows literals.
 * It returns an abstract set of Knowledge Rules that represent them.
 * It is only necessary to call it before the game starts
 *
 * @author Filipe Sena
 *
 */
public final class KnowledgeTermsProcessor {

	public static Set<KnowledgeRule> getKnowledgeTerms(List<Role> roles, List<Gdl> description)
			throws GdlFormatException, SymbolFormatException, InterruptedException {

		// Get the domain of every rule
		ImmutableSentenceDomainModel domainModel = SentenceDomainModelFactory.createWithCartesianDomains(description);
		Iterator<Gdl> itGdl = description.iterator();
		Set<GdlSentence> knowsSet = new HashSet<GdlSentence>();

		while (itGdl.hasNext()) { // Go through all rules
			Gdl next = itGdl.next();

			if (next instanceof GdlRule) { // keyword knows only appears in the
											// body of the rules

				List<GdlLiteral> body = ((GdlRule) next).getBody();
				Iterator<GdlLiteral> itBody = body.iterator();

				while (itBody.hasNext()) { // Searching for the right term
					GdlLiteral bodyN = itBody.next();
					if (bodyN instanceof GdlSentence) {
						GdlSentence possibleKnows = ((GdlSentence) bodyN);
						if (possibleKnows.getName().equals(GdlPool.KNOWS)) {
							knowsSet.add(possibleKnows);
						}
					}
				}
			}
		}

		return getKnowledgeTerms(knowsSet, roles, domainModel);
	}

	// Known bugs: (several grounding variables)
	private static Set<KnowledgeRule> getKnowledgeTerms(Set<GdlSentence> knowsSet, List<Role> roles,
			ImmutableSentenceDomainModel domainModel) throws GdlFormatException, SymbolFormatException {

		Set<KnowledgeRule> toRuturn = new HashSet<KnowledgeRule>();

		for (GdlSentence knows : knowsSet) { // Through all the rules found

			GdlTerm rel;

			if (knows.arity() == 1) { // Common knowledge
				if (knows.isGround()) { // Already grounded
					toRuturn.add(new CommonKnowledge(knows));
					continue;
				} else { // Not grounded
					rel = knows.get(0);

					// Get the sentence form of the relation inside the knows term, to search for his domain
					SentenceForm sf = domainModel.getSentenceForm(rel.toSentence());

					// Get the domain of the relation
					SentenceFormDomain domain = domainModel.getDomain(sf);

					// Get the domain of the relation
					Set<GdlConstant> knowsRelationDomain = domain.getDomainForSlot(0);

					for (GdlConstant possibility : knowsRelationDomain) { // Ground it
						Gdl knowsTermGrounded = GdlFactory
								.create("( knows " + sf.toString().replace("_", possibility.toString()) + " )");
						if (knowsTermGrounded instanceof GdlSentence)
							// Create the Common Knowledge
							toRuturn.add(new CommonKnowledge((GdlSentence) knowsTermGrounded));
					}
				}

			} else {
				if (knows.isGround()) {
					toRuturn.add(new PersonalKnowledge(knows));
					continue;
				} else { //Bugged doesn't work for more variables
					GdlTerm var = knows.get(0);
					rel = knows.get(1); // Personal knowledge

					System.out.println(rel+ " ; " + var + "->" + rel.toString().contains(var.toString()) );
					// Where the rules are of the type (knows ?r (some ?r)) => ?r should be always the same
					boolean sanitize = rel.toString().contains(var.toString());

					SentenceForm sf = domainModel.getSentenceForm(rel.toSentence());
					SentenceFormDomain domain = domainModel.getDomain(sf);
					Set<GdlConstant> knowsRelationDomain = domain.getDomainForSlot(0);
					//Set<GdlConstant> knowsRelationDomain1 = domain.getDomainForSlot(1);

					//System.out.println(knowsRelationDomain);

					for (Role r : roles) {
						if (!r.getName().equals(GdlPool.RANDOM)) {
							for (GdlConstant possibility : knowsRelationDomain) {
								if(sanitize && !possibility.toString().equals(r.toString())) {
									// Just jump the cycle when we are searching for the possibilies and it's not one we want
									continue;
								}
								Gdl knowsTermGrounded = GdlFactory.create("( knows " + r.toString() + " "
										+ sf.toString().replace("_", possibility.toString()) + " )");
								if (knowsTermGrounded instanceof GdlSentence) {
									// Create the Personal Knowledge
									toRuturn.add(new PersonalKnowledge((GdlSentence) knowsTermGrounded));
								}
							}
						}
					}
				}
			}
		}
		return toRuturn;
	}

}
