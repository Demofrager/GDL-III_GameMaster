package sena.base.knowledge;

import org.ggp.base.util.gdl.grammar.GdlConstant;
import org.ggp.base.util.gdl.grammar.GdlSentence;
import org.ggp.base.util.gdl.grammar.GdlTerm;
import org.ggp.base.util.statemachine.Role;

public class PersonalKnowledge extends KnowledgeRule {
	private Role role;
	private GdlTerm target;

	public PersonalKnowledge(GdlSentence groundRule) {
		super(groundRule);
		GdlConstant roleName = (GdlConstant)groundRule.get(0);
		target = groundRule.get(1);

		role = new Role(roleName);
	}

	public Role getRole(){
		return role;
	}

	public GdlTerm getTarget(){
		return target;
	}

}

