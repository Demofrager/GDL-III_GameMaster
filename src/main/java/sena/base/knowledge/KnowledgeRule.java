package sena.base.knowledge;

import org.ggp.base.util.gdl.grammar.GdlSentence;

public abstract class KnowledgeRule {

	private GdlSentence sentence;

	public KnowledgeRule (GdlSentence sentence){
		this.sentence=sentence;
	}

	public GdlSentence getSentence() {
		return sentence;
	}

	@Override
	public String toString(){
		return sentence.toString();
	}
}
