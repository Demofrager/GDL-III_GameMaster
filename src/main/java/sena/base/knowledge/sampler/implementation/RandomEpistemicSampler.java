package sena.base.knowledge.sampler.implementation;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.ggp.base.util.gdl.grammar.GdlPool;
import org.ggp.base.util.gdl.grammar.GdlSentence;
import org.ggp.base.util.statemachine.MachineState;
import org.ggp.base.util.statemachine.Move;
import org.ggp.base.util.statemachine.Role;
import org.ggp.base.util.statemachine.exceptions.MoveDefinitionException;
import org.ggp.base.util.statemachine.exceptions.TransitionDefinitionException;

import sena.base.knowledge.CommonKnowledge;
import sena.base.knowledge.KnowledgeRule;
import sena.base.knowledge.PersonalKnowledge;
import sena.base.knowledge.bag.Bag;
import sena.base.knowledge.bag.exception.InconsistentPathToCurrentLvlException;
import sena.base.knowledge.bag.exception.ModelNotFoundException;
import sena.base.knowledge.indistinguishability.AccessibilityRelationStructure;
import sena.base.knowledge.model.Model;
import sena.base.knowledge.sampler.DynamicEpistemicSampler;
import sena.base.knowledge.verifier.KnowledgeVerifier;
import sena.base.util.path.PathUtilities;
import sena.base.util.statemachine.III_StateMachine;
import sena.base.util.statemachine.Percept;
import sena.base.util.statemachine.exceptions.PerceptDefinitionException;

public class RandomEpistemicSampler extends DynamicEpistemicSampler {


	private III_StateMachine stateMachine;
	private MachineState intialState;

	private List<Role> roles;


	public RandomEpistemicSampler(III_StateMachine stateMachine, List<Role> roles, int numberOfModels,
			MachineState initialState) {

		this.roles = roles;
		this.stateMachine = stateMachine;
		this.intialState = initialState.clone();

		setSamplerRound(0);
		setBag( new Bag(numberOfModels, this.intialState));
	}


	@Override
	public List<Model> sample(List<Move> recentMoves, List<Integer> inconsistentModelsIDs) throws ModelNotFoundException, MoveDefinitionException,
			TransitionDefinitionException, InconsistentPathToCurrentLvlException, PerceptDefinitionException {

		List<Model> aux = new ArrayList<Model>(getBagSize());
		for (int i = 0; i < getBagSize(); i++) {
			//if(!inconsistentModelsIDs.contains(i)) // Avoid unnecessary exploration. Probably troublesome
				aux.add(sample(i));
		}
		setSamplerRound(getSamplerRound() + 1);

		List<Model> toReturn = new ArrayList<Model>(aux); // Don't want to pass
															// pointers to the
															// models inside the
															// list. Just a hard
															// copy.
		return toReturn;
	}

	private Model sample(int modelID) throws ModelNotFoundException, MoveDefinitionException,
			TransitionDefinitionException, PerceptDefinitionException {

		Model toUpdate = getModel(modelID);
		MachineState stateToUpdate = toUpdate.getModel().clone();
		List<List<Move>> pathToUpdate = new ArrayList<List<Move>>(toUpdate.getModelID());

		List<Move> jointMove = stateMachine.getRandomJointMove(stateToUpdate);

		List<List<Percept>> jointPercepts = stateMachine.getPercepts(stateToUpdate, jointMove);
		List<List<List<Percept>>> modelPerceptPathtoUpdate = PathUtilities
				.perceptsPathAppend(toUpdate.getModelPerceptHistory(), jointPercepts);

		stateToUpdate = nextStateWithKnowledgeInertia(stateToUpdate, jointMove);

		pathToUpdate.add(jointMove);

		return updateModel(toUpdate, stateToUpdate, pathToUpdate, modelPerceptPathtoUpdate);
	}

	@Override
	public List<Model> resample(List<Integer> modelIDs, int level, List<Move> leveledJointMoves, Map<Role, AccessibilityRelationStructure> indMatrices)
			throws ModelNotFoundException, InconsistentPathToCurrentLvlException, MoveDefinitionException,
			TransitionDefinitionException, PerceptDefinitionException {
		List<Model> newModels = new ArrayList<Model>(getBagSize());

		for (int i : modelIDs) {
			newModels.add(resample(i, level, indMatrices));
		}

		return newModels;
	}

	private Model resample(int modelID, int level, Map<Role, AccessibilityRelationStructure> indMatrices) throws ModelNotFoundException, InconsistentPathToCurrentLvlException,
			MoveDefinitionException, TransitionDefinitionException, PerceptDefinitionException {

		Model toResample = getModel(modelID);
		MachineState state;

		if (level == 0)
			state = getModelKnowledge(modelID, intialState.clone(), indMatrices).clone();
		else
			state = toResample.getModel().clone();

		List<List<Move>> newStatePath = new ArrayList<List<Move>>(level + 1);
		List<List<List<Percept>>> modelPerceptPath = new ArrayList<List<List<Percept>>>(level + 1);

		if (level > 0) {
			for (int i = 0; i < level; i++) {
				newStatePath.add(toResample.getModelID().get(i));
				modelPerceptPath.add(toResample.getModelPerceptHistory().get(i));
			}
		}

		List<Move> jointMove = stateMachine.getRandomJointMove(state);

		newStatePath.add(jointMove);
		modelPerceptPath.add(stateMachine.getPercepts(state, jointMove));
		state = nextStateWithKnowledgeInertia(state, jointMove);

		return updateModel(toResample, state, newStatePath, modelPerceptPath);
	}

	// AUXILIAR METHODS
	private MachineState nextStateWithKnowledgeInertia(MachineState state, List<Move> jointMove)
			throws TransitionDefinitionException {

		Set<GdlSentence> contents = new HashSet<>(state.getContents());

		MachineState aux = stateMachine.getNextState(state, jointMove);

		Set<GdlSentence> newContents = new HashSet<>();
		for (GdlSentence sentence : contents) {
			if (sentence.getName().equals(GdlPool.KNOWS)) {
				newContents.add(sentence);
			}
		}

		newContents.addAll(aux.getContents());
		aux = new MachineState(newContents);

		return aux;

	}

	private MachineState getModelKnowledge(int modelID, MachineState state, Map<Role, AccessibilityRelationStructure> indMatrices)
			throws ModelNotFoundException {
		Set<GdlSentence> knowledgeStage = new HashSet<GdlSentence>();

		for (KnowledgeRule knows : targetKnowsRules) {
			if (knows instanceof PersonalKnowledge) {
				PersonalKnowledge target = (PersonalKnowledge) knows;
				if (KnowledgeVerifier.verifyKnowledge(target, modelID, state, indMatrices, stateMachine, this))
					knowledgeStage.add(target.getSentence());
			} else {
				CommonKnowledge target = (CommonKnowledge) knows;
				if (KnowledgeVerifier.verifyKnowledge(target, modelID, state, indMatrices, stateMachine, this))
					knowledgeStage.add(target.getSentence());
			}
		}

		knowledgeStage.addAll(state.getContents());
		MachineState newEpistemicState = new MachineState(knowledgeStage);
		return newEpistemicState;
	}

	/**
	 * Updates the specific model with the given information.
	 *
	 * @param model
	 * @param newState
	 * @param newPath
	 * @param modelPerceptPath
	 * @return
	 */
	private Model updateModel(Model model, MachineState newState, List<List<Move>> newPath,
			List<List<List<Percept>>> modelPerceptPath) {
		model.setModel(newState);
		model.setModelID(newPath);
		model.setModelPerceptHistory(modelPerceptPath);
		return model;
	}

	@Override
	public void updateMState(Map<Role, AccessibilityRelationStructure> indMatrices) throws ModelNotFoundException {

		for (int i = 0; i < getBagSize(); i++) {
			Model model = getModel(i);
			MachineState newEpistemicModel = getModelKnowledge(i, model.getModel(), indMatrices);

			updateModel(model, newEpistemicModel, model.getModelID(), model.getModelPerceptHistory());
		}
	}

	@Override
	public void updateMState(Map<Role, AccessibilityRelationStructure> currentMatrices, List<Integer> resampleModels) throws ModelNotFoundException {


		for(int i: resampleModels){
			Model model = getModel(i);
			MachineState newEpistemicModel = getModelKnowledge(i, model.getModel(), currentMatrices);

			updateModel(model, newEpistemicModel, model.getModelID(), model.getModelPerceptHistory());
		}
	}

}
