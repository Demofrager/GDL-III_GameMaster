package sena.base.knowledge.sampler;

import java.util.List;
import java.util.Map;
import java.util.Set;

import org.ggp.base.util.statemachine.Move;
import org.ggp.base.util.statemachine.Role;
import org.ggp.base.util.statemachine.exceptions.MoveDefinitionException;
import org.ggp.base.util.statemachine.exceptions.TransitionDefinitionException;

import sena.base.knowledge.KnowledgeRule;
import sena.base.knowledge.bag.exception.InconsistentPathToCurrentLvlException;
import sena.base.knowledge.bag.exception.ModelNotFoundException;
import sena.base.knowledge.indistinguishability.AccessibilityRelationStructure;
import sena.base.knowledge.model.Model;
import sena.base.util.statemachine.exceptions.PerceptDefinitionException;
/**
 * This class represents a Dynamic Epistemic Sampler. It extends a sampler,
 * where models are kept and updated.
 *
 * In a Dynamic Epistemic Sampler the models need to be updated epistemically
 * round by round. Therefore it is necessary to have methods that can be called
 * to update the epistemic part of our mstate.
 *
 * @author Filipe Sena
 *
 */
public abstract class DynamicEpistemicSampler extends Sampler {

	// Knowns relations identified that we need to test if are true or not
	protected Set<KnowledgeRule> targetKnowsRules;

	/**
	 * Generates a new joint move to update the models.
	 * @param recentMoves - moves of the players
	 * @param inconsistentModelsIDs - ID of the inconsistent models
	 * @return the list of updated models
	 * @throws ModelNotFoundException
	 * @throws MoveDefinitionException
	 * @throws TransitionDefinitionException
	 * @throws InconsistentPathToCurrentLvlException
	 * @throws PerceptDefinitionException
	 */
	public abstract List<Model> sample(List<Move> recentMoves, List<Integer> inconsistentModelsIDs)
			throws ModelNotFoundException, MoveDefinitionException,
			TransitionDefinitionException, InconsistentPathToCurrentLvlException, PerceptDefinitionException;

	/**
	 * Generates a new joint move for the model given by the ID and the level
	 * @param modelIDs - ID of the models to re-sample
	 * @param round - round of the game
	 * @param leveledJointMoves - list of joint moves until that round
	 * @param structure - structure of the given round (only used for the first round really,
		but has to be passed for every round - to make code simple)
	 * @return the list of models re-sampled
	 * @throws ModelNotFoundException
	 * @throws InconsistentPathToCurrentLvlException
	 * @throws MoveDefinitionException
	 * @throws TransitionDefinitionException
	 * @throws PerceptDefinitionException
	 */
	public abstract List<Model> resample(List<Integer> modelIDs, int round, List<Move> leveledJointMoves,
			Map<Role, AccessibilityRelationStructure> structure)
			throws ModelNotFoundException, InconsistentPathToCurrentLvlException,
			MoveDefinitionException, TransitionDefinitionException, PerceptDefinitionException;

	/**
	 * Update the epistemic part of all the models, given the current Kripke structure.
	 * Used after the sample method and after the update of the given structure.
	 * @param currentStructure - Kripke structure of the current round
	 * @throws ModelNotFoundException
	 */
	public abstract void updateMState(Map<Role, AccessibilityRelationStructure> currentStructure) throws ModelNotFoundException;

	/**
	 * Updates the epistemic part of a list of models, given a Kripke structure of a specific round.
	 * Used after the re-sample method and after the update of the given structure.
	 * @param roundStructure - kripke structure of a specific round
	 * @param inconsistentModelsIDs - IDs of the models that were re-sampled (only those are updated)
	 * @throws ModelNotFoundException
	 */
	public abstract void updateMState(Map<Role, AccessibilityRelationStructure> roundStructure, List<Integer> inconsistentModelsIDs)
			throws ModelNotFoundException;

	/**
	 * Sets the target knowledge rules necessary to update the mstate epistemically
	 * @param targetKnowsRules - knows rules identified as necessary to test if each player
	 * knows it.
	 */
	public void setTargetKnowsRules(Set<KnowledgeRule> targetKnowsRules) {
		this.targetKnowsRules = targetKnowsRules;
	}


}
