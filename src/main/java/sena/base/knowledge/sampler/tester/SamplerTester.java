package sena.base.knowledge.sampler.tester;

import java.io.IOException;
import java.util.List;

import javax.naming.SizeLimitExceededException;

import org.ggp.base.util.game.Game;
import org.ggp.base.util.game.GameRepository;
import org.ggp.base.util.gdl.factory.GdlFactory;
import org.ggp.base.util.gdl.factory.exceptions.GdlFormatException;
import org.ggp.base.util.gdl.grammar.Gdl;
import org.ggp.base.util.statemachine.MachineState;
import org.ggp.base.util.statemachine.Move;
import org.ggp.base.util.statemachine.Role;
import org.ggp.base.util.statemachine.exceptions.GoalDefinitionException;
import org.ggp.base.util.statemachine.exceptions.MoveDefinitionException;
import org.ggp.base.util.statemachine.exceptions.TransitionDefinitionException;
import org.ggp.base.util.symbol.factory.exceptions.SymbolFormatException;

import sena.base.knowledge.bag.exception.ModelNotFoundException;
import sena.base.util.statemachine.Percept;
import sena.base.util.statemachine.exceptions.PerceptDefinitionException;
import sena.base.util.statemachine.implementation.prover.III_ProverStateMachine;

public class SamplerTester {


	public static void main(String[] args) throws SecurityException, IOException, MoveDefinitionException,
			TransitionDefinitionException, PerceptDefinitionException, GdlFormatException, SymbolFormatException,
			GoalDefinitionException, InterruptedException, ModelNotFoundException, SizeLimitExceededException {

		Game game = GameRepository.getDefaultRepository().getGame("numberGuessingEpistemic32");
		//Game game =  GameRepository.getDefaultRepository().getGame("muddyChildren3");
		List<Role> roles = Role.computeRoles(game.getRules());

		III_ProverStateMachine stateMachine = new III_ProverStateMachine();
		List<Gdl> gameRules = game.getRules();

		stateMachine.initialize(gameRules);

		MachineState currentState = stateMachine.getInitialState();
		List<Move> jointMove;
		List<List<Percept>> jointPercepts;
		while(!stateMachine.isTerminal(currentState)){

			jointMove = stateMachine.getRandomJointMove(currentState);

			jointPercepts = stateMachine.getPercepts(currentState, jointMove);

			System.out.println("This is the current state:\n"  + currentState);
			System.out.println("This is the joint move selected by the players:\n"  + jointMove);
			System.out.println("This is the percepts  that the player will recieve in the next state:\n"  + jointPercepts);

			currentState = stateMachine.getNextState(currentState, jointMove);
			System.out.println("This is the next state:\n" + currentState);

			if(!jointMove.get(0).equals(new Move(GdlFactory.createTerm("noop")))) ;// TODO: Need to add this to III_GameServer
				//stateMachine.setupTargetKnowledge(currentState, gameRules);

			//stateMachine.updateKnowledgeStage(jointMove, jointPercepts);
		}

		System.exit(0);
	}

}
