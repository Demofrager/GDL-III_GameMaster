package sena.base.knowledge.statemachine.implementation;

import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

import javax.naming.SizeLimitExceededException;

import org.ggp.base.util.gdl.grammar.GdlSentence;
import org.ggp.base.util.statemachine.MachineState;
import org.ggp.base.util.statemachine.Move;
import org.ggp.base.util.statemachine.Role;
import org.ggp.base.util.statemachine.exceptions.MoveDefinitionException;
import org.ggp.base.util.statemachine.exceptions.TransitionDefinitionException;

import sena.base.knowledge.CommonKnowledge;
import sena.base.knowledge.KnowledgeRule;
import sena.base.knowledge.PersonalKnowledge;
import sena.base.knowledge.bag.exception.InconsistentPathToCurrentLvlException;
import sena.base.knowledge.bag.exception.ModelNotFoundException;
import sena.base.knowledge.indistinguishability.AccessabilityRelationManager;
import sena.base.knowledge.sampler.DynamicEpistemicSampler;
import sena.base.knowledge.sampler.implementation.PerspectiveShiftingDynamicEpistemicSampler;
import sena.base.knowledge.sampler.implementation.RandomEpistemicSampler;
import sena.base.knowledge.statemachine.KnowledgeStateMachine;
import sena.base.knowledge.verifier.KnowledgeVerifier;
import sena.base.util.statemachine.III_StateMachine;
import sena.base.util.statemachine.Percept;
import sena.base.util.statemachine.exceptions.PerceptDefinitionException;

public class DynamicEpistemicStateMachine extends KnowledgeStateMachine {

	private int NUMBER_OF_MODELS = 50;
	private int INCONSISTENT_MODELS = 40;
	//private static final int NUMBER_OF_MODELS = 100;
	//private static final int NUMBER_OF_MODELS = 150;

	private static final boolean PERSPECTIVE_SHIFTING_SAMPLING = true;
	private static final int MAX_RESAMPLE_TRIES = 30;

	// For knowledge terms calculation
	private Set<KnowledgeRule> targetKnowsRules;

	private AccessabilityRelationManager indistinguishabilityManager;
	private DynamicEpistemicSampler sampler;

	private III_StateMachine stateMachine;

	private List<List<Move>> actualMoveHistory;
	private List<List<List<Percept>>> actualPerceptHistory;

	public DynamicEpistemicStateMachine(III_StateMachine stateMachine, List<Role> roles,
			MachineState initialState, int nrModels, int inconsistent) {

		System.out.println("Perspective Shifting: " + PERSPECTIVE_SHIFTING_SAMPLING);
		this.stateMachine = stateMachine;
		this.NUMBER_OF_MODELS = nrModels;
		this.INCONSISTENT_MODELS = inconsistent;

		new KnowledgeVerifier(roles);
		indistinguishabilityManager = new AccessabilityRelationManager(roles, NUMBER_OF_MODELS);

		if (PERSPECTIVE_SHIFTING_SAMPLING) {
			sampler = new PerspectiveShiftingDynamicEpistemicSampler(stateMachine, roles, NUMBER_OF_MODELS,
					initialState);
		} else
			sampler = new RandomEpistemicSampler(stateMachine, roles, NUMBER_OF_MODELS, initialState);

		actualMoveHistory = new LinkedList<List<Move>>();
		actualPerceptHistory = new LinkedList<List<List<Percept>>>();

	}

	@Override
	public void setTargetKnowsRules(Set<KnowledgeRule> targetKnowsRules) {
		this.targetKnowsRules = targetKnowsRules;

		sampler.setTargetKnowsRules(targetKnowsRules);
	}


	@Override
	public void updateNextTurnKnowledge(List<Move> previousMoves,
			List<List<Percept>> percepts) throws MoveDefinitionException, PerceptDefinitionException,
			TransitionDefinitionException, SizeLimitExceededException {

		//int resample_tries = MAX_RESAMPLE_TRIES;
		actualMoveHistory.add(previousMoves);
		actualPerceptHistory.add(percepts);

		// Change this to a global variable
		List<Integer> inconsistentModelsIDs = new LinkedList<Integer>();
		try {
			indistinguishabilityManager.updateCurrentAccessabilityRelation(actualMoveHistory, actualPerceptHistory,
					sampler.sample(previousMoves, inconsistentModelsIDs));

			sampler.updateMState(indistinguishabilityManager.getCurrentStructure());
			//System.out.println(sampler.toString());
			inconsistentModelsIDs = KnowledgeVerifier.verifyInconsistency(indistinguishabilityManager.getCurrentStructure());
			//System.out.println(inconsistentModelsIDs);
			//System.out.println(indistinguishabilityManager.toString());
			while (inconsistentModelsIDs.size() > INCONSISTENT_MODELS /*&& resample_tries > 0*/)  {
				// TODO: Stop the cycle after some iterations, to ensure no deadloops
				//System.out.println("Re sampling(" /*+ resample_tries */+ "):" + inconsistentModelsIDs.size() + " / " + INCONSISTENT_MODELS);
				resample(inconsistentModelsIDs);
				inconsistentModelsIDs = KnowledgeVerifier.verifyInconsistency(indistinguishabilityManager.getCurrentStructure());
				//System.out.println(sampler.toString());
				//System.out.println(indistinguishabilityManager.toString());
				//System.out.println(inconsistentModelsIDs);
				//resample_tries --;
			}

		} catch (ModelNotFoundException e) {
			e.printStackTrace();
		} catch (InconsistentPathToCurrentLvlException e) {
			e.printStackTrace();
		}
	}

	private void resample(List<Integer> inconsistentModelsIDs)
			throws ModelNotFoundException, InconsistentPathToCurrentLvlException, MoveDefinitionException,
			TransitionDefinitionException, PerceptDefinitionException, SizeLimitExceededException {

		int samplerRound = sampler.getSamplerRound();

		// Start sampling from the initial round until the current round.
		for (int currentResampleRound = 0; currentResampleRound < samplerRound; currentResampleRound++) {
			// System.out.println("Current level:" + currentResampleLvl);

			sampler.resample(inconsistentModelsIDs, currentResampleRound,
						actualMoveHistory.get(currentResampleRound), indistinguishabilityManager.getStructure(currentResampleRound));

			//System.out.println(sampler.toString());
			indistinguishabilityManager.updateAccessabilityRelation(actualMoveHistory, actualPerceptHistory, currentResampleRound,
					sampler.getAllModels());

			sampler.updateMState(indistinguishabilityManager.getStructure(currentResampleRound), inconsistentModelsIDs);
		}
	}

	@Override
	public Set<GdlSentence> getTurnKnowledge(MachineState currentState) throws ModelNotFoundException, TransitionDefinitionException {
		Set<GdlSentence> knowledgeStage = new HashSet<GdlSentence>();
		//System.out.println(sampler.toString());
		//System.out.println(indistinguishabilityManager.getCurrentStructure().toString());
		for (KnowledgeRule knows : targetKnowsRules) {
			if (knows instanceof PersonalKnowledge) {
				PersonalKnowledge target = (PersonalKnowledge) knows;
				if (KnowledgeVerifier.verifyKnowledge(target, currentState, indistinguishabilityManager.getCurrentStructure(),
						stateMachine, sampler))
					knowledgeStage.add(target.getSentence());
			} else {
				CommonKnowledge target = (CommonKnowledge) knows;
				if (KnowledgeVerifier.verifyKnowledge(target, currentState, indistinguishabilityManager.getCurrentStructure(),
						stateMachine, sampler))
					knowledgeStage.add(target.getSentence());
			}
		}

		return knowledgeStage;
	}
}