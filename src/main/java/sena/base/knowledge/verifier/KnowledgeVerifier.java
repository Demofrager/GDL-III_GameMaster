package sena.base.knowledge.verifier;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.Stack;

import org.ggp.base.util.gdl.grammar.GdlPool;
import org.ggp.base.util.gdl.grammar.GdlRelation;
import org.ggp.base.util.gdl.grammar.GdlTerm;
import org.ggp.base.util.statemachine.MachineState;
import org.ggp.base.util.statemachine.Role;

import sena.base.knowledge.CommonKnowledge;
import sena.base.knowledge.PersonalKnowledge;
import sena.base.knowledge.bag.exception.ModelNotFoundException;
import sena.base.knowledge.indistinguishability.AccessibilityRelationStructure;
import sena.base.knowledge.model.Model;
import sena.base.knowledge.sampler.DynamicEpistemicSampler;
import sena.base.util.statemachine.III_StateMachine;

/**
 * This class implements the semantics of Dynamic Epistemic Logic.
 * Uses a kripke structure to calculate the possible knowledge targets
 * existent in the game. It also verifies the consistency (transitive
 * closure operation) of the given kripke structures.
 *
 * @author Filipe Sena
 */
public final class KnowledgeVerifier {

	private static List<Role> roles;

	// Must have roles to verify knowledge.
	public KnowledgeVerifier(List<Role> roles) {
		KnowledgeVerifier.roles = roles;
	}

	/**
	 * For the actual state of the game.
	 * Applies the DEL semantics of the personal knowledge target.
	 * It is checks the major diagonal of the Accessibility Relation
	 * Structure belonging to the player that the target rule belongs to.
	 * Verifies if the knowledge rule is satisfied.
	 * Bare in mind that if no model is found useful, then is used the
	 * given the provided machine state (the real state of the game).
	 *
	 * @param target - knowledge rule to check if it is true at the current state
	 * 		and using the kripke structure;
	 * @param actualState - the state to be tested if no useful models are found;
	 * @param kripkeStructure - the kripke structure of the current round;
	 * @param stateMachine - a state machine to help prove the relation inside
	 * 		the mstates
	 * @param sampler - to get the necessary models
	 * @return if the target rule is satisfied or not in the current structure
	 * @throws ModelNotFoundException
	 */
	public static boolean verifyKnowledge(PersonalKnowledge target, MachineState actualState,
			Map<Role, AccessibilityRelationStructure> kripkeStructure, III_StateMachine stateMachine,
			DynamicEpistemicSampler sampler) throws ModelNotFoundException {

		GdlTerm toKnow = target.getTarget();
		boolean doesHeKnow = stateMachine.proveRelation((GdlRelation) toKnow.toSentence(), actualState); // Changed 10/jan

		AccessibilityRelationStructure m = kripkeStructure.get(target.getRole());
		List<Integer> usefulModelsIDs = m.getAllUsefulModelsIds();

		if (!usefulModelsIDs.isEmpty()) {
			// At least one model ~ from the actual play of the game
			List<Model> usefulModels = sampler.getModelsById(usefulModelsIDs);
			for (Model model : usefulModels) {
				doesHeKnow &= stateMachine.proveRelation((GdlRelation) toKnow.toSentence(), model.getModel());
			}
		} else // use the actual state of the game
			return stateMachine.proveRelation((GdlRelation) toKnow.toSentence(), actualState);

		return doesHeKnow;
	}

	/**
	 * For models.
	 * Applies the DEL semantics of the personal knowledge target.
	 * It is checks for the indistinguishable (~) models of the Accessibility Relation
	 * Structure belonging to the player that the target rule belongs to.
	 * Verifies if the knowledge rule is satisfied in those models.
	 * Bare in mind that if no model is found indistinguishable, then is used the
	 * given the provided mstate, that is supposed to be the contents of
	 * the model given by the provided ID, to see if the target is true, like in
	 * the semantics of the DEL: Every model is ~ between itself.
	 *
	 * @param target - knowledge rule to check if it is true at the given model
	 * 		and using the kripke structure;
	 * @param modelID - the model id;
	 * @param mstate - mstate of the model with the given ID;
	 * @param kripkeStructure - the kripke structure of a round;
	 * @param stateMachine - a state machine to help prove the relation inside
	 * 		the mstates;
	 * @param sampler - to get the necessary models;
	 * @return if the target rule is satisfied or not in the model with given
	 * 		kripke structure of a round.
	 * @throws ModelNotFoundException
	 */
	public static boolean verifyKnowledge(PersonalKnowledge target, int modelID, MachineState mstate,
			Map<Role, AccessibilityRelationStructure> kripkeStructure, III_StateMachine stateMachine,
			DynamicEpistemicSampler sampler) throws ModelNotFoundException {

		GdlTerm toKnow = target.getTarget();

		boolean result = stateMachine.proveRelation((GdlRelation) toKnow.toSentence(), mstate);

		AccessibilityRelationStructure m = kripkeStructure.get(target.getRole());
		List<Integer> indistinguishableModelsIDs;
		List<Model> indistinguishableModels;

		indistinguishableModelsIDs = m.getAllIndistinguishableModelsIds(modelID);

		if (!indistinguishableModelsIDs.isEmpty()) {
			indistinguishableModels = sampler.getModelsById(indistinguishableModelsIDs);
			for (Model model : indistinguishableModels) {
				result &= stateMachine.proveRelation((GdlRelation) toKnow.toSentence(), model.getModel());
			}
		}
		return result;
	}

	/**
	 * For the actual state of the game.
	 * Applies the DEL semantics of the common knowledge target.
	 * It is checks the transitive closure (~+) for all the models
	 * of the Accessibility Relation Structure (consistency).
	 * Verifies if the knowledge rule is satisfied in the ~+ set.
	 * Bare in mind that if no model is found consistent, then is used the
	 * given the provided state.
	 *
	 * @param target - knowledge rule to check if it is true at the current state
	 * 		and using the kripke structure;
	 * @param actualState - the state to be tested if no useful models are found;
	 * @param kripkeStructure - the kripke structure of the current round;
	 * @param stateMachine - a state machine to help prove the relation inside
	 * 		the mstates
	 * @param sampler - to get the necessary models
	 * @return if the target rule is satisfied or not in the state with the current structure
	 * @throws ModelNotFoundException
	 */
	public static boolean verifyKnowledge(CommonKnowledge target, MachineState actualState,
			Map<Role, AccessibilityRelationStructure> kripkeStructure, III_StateMachine stateMachine,
			DynamicEpistemicSampler sampler) throws ModelNotFoundException {

		GdlTerm toKnow = target.getTarget();
		boolean doesHeKnow = stateMachine.proveRelation((GdlRelation) toKnow.toSentence(), actualState); // Changed 10/Jan

		List<Integer> consistentModelsIDs = verifyConsistency(kripkeStructure);
		if (!consistentModelsIDs.isEmpty()) {
			// At least one model ~ from the actual play of the game
			List<Model> usefulModels = sampler.getModelsById(consistentModelsIDs);
			for (Model model : usefulModels) {
				doesHeKnow &= stateMachine.proveRelation((GdlRelation) toKnow.toSentence(), model.getModel());
			}
		} else // use the actual state of the game
			return stateMachine.proveRelation((GdlRelation) toKnow.toSentence(), actualState);

		return doesHeKnow;
	}

	/** UNTESTED: no games where legal and sees rules
	 * depend on common knowledge.
	 *
	 * For models.
	 * Applies the DEL semantics of the common knowledge target.
	 * It is checks the transitive closure (~+) of that model in the
	 * Kripke structure provided. Verifies if the knowledge rule is
	 * satisfied in all ~+ models.
	 * Bare in mind that if no model is found in the transitive closure
	 * set, then is used the given the provided mstate.
	 *
	 * @param target - knowledge rule to check if it is true at the given model
	 * 		and using the kripke structure;
	 * @param modelID - the model id;
	 * @param mstate - mstate of the model with the given ID;
	 * @param kripkeStructure - the kripke structure of a round;
	 * @param stateMachine - a state machine to help prove the relation inside
	 * 		the mstates;
	 * @param sampler - to get the necessary models;
	 * @return if the target rule is satisfied or not in the model with given
	 * 		kripke structure of a round.
	 * @throws ModelNotFoundException
	 */
	public static boolean verifyKnowledge(CommonKnowledge target, int modelID, MachineState mstate,
			Map<Role, AccessibilityRelationStructure> kripkeStructure, III_StateMachine stateMachine,
			DynamicEpistemicSampler sampler) throws ModelNotFoundException {

			Set<Integer> closure = applyTransitiveClosure(modelID, kripkeStructure);
			boolean doesHeKnow = true;
			GdlTerm toKnow = target.getTarget();

			if (!closure.isEmpty()) {
				// At least one model ~ from the actual play of the game
				List<Model> usefulModels = sampler.getModelsById(new ArrayList<>(closure));
				for (Model model : usefulModels) {
					doesHeKnow &= stateMachine.proveRelation((GdlRelation) toKnow.toSentence(), model.getModel());
				}
			} else // use the actual state of the game
				return stateMachine.proveRelation((GdlRelation) toKnow.toSentence(), mstate);

		return false;
	}

	/**
	 * Verifies the models are in the transitive closure set of
	 * the state of the game. That's why we verify if a model is consistent.
	 * If one is consistent, then all are consistent.
	 * The cycle is just to start at the first model and go one by one until you
	 * find one that is useful. Since it is a DFS, once you find one it shouldn't
	 * be necessary to continue to search more because DFS automatically explores
	 * everything.
	 *
	 * @param kripkeStructure - the kripke structure of a round;
	 * @return the list of id's of the consistent models
	 */
	public static List<Integer> verifyConsistency(Map<Role, AccessibilityRelationStructure> kripkeStructure) {
		Set<Integer> aux = new HashSet<Integer>();
		int numberOfModels = kripkeStructure.get(roles.get(0)).getNumberOfModels();

		Set<Integer> closure = new HashSet<>(numberOfModels);
		AccessibilityRelationStructure m = null;

		for (int modelIndex = 0; modelIndex < numberOfModels; modelIndex++) {
			// Depth-first-search
			closure = applyTransitiveClosure(modelIndex, kripkeStructure);

			boolean consistent = false;
			for (int i : closure) {
				// Check if any of the models is connected to the actual play of the game
				for (Role role : roles) {
					if (!role.getName().equals(GdlPool.RANDOM)) {
						m = kripkeStructure.get(role);
						consistent |= m.getUsefulNess(i);
					}
				}
			}

			if (consistent) { // Fixed this
				aux.addAll(closure);
			}
			closure = new HashSet<>(numberOfModels);
		}
		return new ArrayList<>(aux);

	}

	/**
	 * Given a model index it will check the for Kripke structure
	 *  the other models of the transitive closure set.
	 *
	 * @param modelID - the model ID
	 * @param kripkeStructure - the structure to test the model
	 * @return the transitive closure set of the given model
	 */
	private static Set<Integer> applyTransitiveClosure(int modelID,
			Map<Role, AccessibilityRelationStructure> kripkeStructure) {

		// Maybe another solution to avoid code redundancy
		Set<Integer> discovered = new HashSet<>(kripkeStructure.get(roles.get(0)).getNumberOfModels());
		Stack<Integer> s = new Stack<Integer>();
		AccessibilityRelationStructure m = null;

		s.push(modelID);
		while (!s.isEmpty()) {
			int index = s.pop();
			if (!discovered.contains(index)) {
				discovered.add(index);

				// Check edges
				for (Role role : roles) {
					if (!role.getName().equals(GdlPool.RANDOM)) {
						m = kripkeStructure.get(role);
						for (int i : m.getAllIndistinguishableModelsIds(index)) {
							s.push(i);
						}
					}
				}
			}
		}
		return discovered;
	}

	/**
	 * This method is the complementary of the verifyConsistency.
	 * A model is inconsistent if is for all the models connected to it,
	 * they all are useless
	 * @return a list of model IDs that are inconsistent with the game rules
	 */
	public static List<Integer> verifyInconsistency(
			Map<Role, AccessibilityRelationStructure> indistinghuishability) {

		LinkedList<Integer> toReturn = new LinkedList<Integer>();
		List<Integer> consistentIDs = verifyConsistency(indistinghuishability);

		// It is complementary with the consistent ones
		for (int modelIndex = 0; modelIndex < indistinghuishability.get(roles.get(0)).getNumberOfModels(); modelIndex++)
			if (!consistentIDs.contains(modelIndex))
				toReturn.add(modelIndex);

		return toReturn;
	}
}
