package sena.base.knowledge.bag;

import java.util.ArrayList;
import java.util.List;

import org.ggp.base.util.statemachine.MachineState;

import sena.base.knowledge.bag.exception.ModelNotFoundException;
import sena.base.knowledge.model.Model;

/**
 * Where the models will be stored
 *
 * @author demofrager
 *
 */
public class Bag {

	private List<Model> models;
	private int size;


	public Bag( int size, MachineState initialState) {
		this.size = size;
		models = new ArrayList<Model>(this.size);

		Model initialModel ;
		for (int i = 0; i < this.size; i++) {
			initialModel = new Model(initialState.clone());
			models.add(initialModel);
		}
	}

	public int size(){
		return size;
	}

	public Model getModel(int modelID) throws ModelNotFoundException{
		if(modelID >= models.size())
			throw new ModelNotFoundException(modelID);

		return models.get(modelID);
	}

	public List<Model> getAll(){
		return models;
	}


	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("Bag [\n");
		for(int i = 0; i < models.size(); i++ )
			sb.append(i + ": " + models.get(i).toString());
		sb.append( "]");

		return  sb.toString();
	}

}
