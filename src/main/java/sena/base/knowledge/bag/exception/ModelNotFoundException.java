package sena.base.knowledge.bag.exception;

public class ModelNotFoundException extends Exception {


	/**
	 *
	 */
	private static final long serialVersionUID = -7000298567496988243L;
	private int modelID;
	public ModelNotFoundException(int modelID) {
		super();
		this.modelID = modelID;
	}

	@Override
	public String toString() {
		return "ModelNotFoundException [modelID=" + modelID + "]";
	}


}
