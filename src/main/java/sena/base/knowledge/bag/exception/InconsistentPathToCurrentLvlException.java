package sena.base.knowledge.bag.exception;

import java.util.List;

import org.ggp.base.util.statemachine.Move;

public class InconsistentPathToCurrentLvlException extends Exception {
	/**
	 *
	 */
	private static final long serialVersionUID = 8951643502803242161L;
	private List<List<Move>> path;

	public InconsistentPathToCurrentLvlException(List<List<Move>> path) {
		super();
		this.path = path;
	}

	@Override
	public String toString() {
		return "InconsistentPathToCurrentLvl [path=" + path + "]";
	}

}
