package sena.base.servers;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.ggp.base.util.game.Game;
import org.ggp.base.util.game.GameRepository;
import org.ggp.base.util.gdl.factory.exceptions.GdlFormatException;
import org.ggp.base.util.statemachine.Role;
import org.ggp.base.util.statemachine.exceptions.GoalDefinitionException;
import org.ggp.base.util.symbol.factory.exceptions.SymbolFormatException;

import sena.base.server.II_GameServer;
import sena.base.util.match.II_Match;

/**
 * Extension of a java class GameServerRunner of the authors:
 * @author Evan Cox
 * @author Sam Schreiber
 *
 * GDL_II_Manager is a utility program that lets you start up a match directly
 * from the command line. It runs an instance of a match, and finishes it.
 *
 * It takes the following arguments: args[0] = tournament name, for storing
 * results args[1] = game key, for loading the game args[2] = start clock, in
 * seconds args[3] = play clock, in seconds args[4,5,6] = host, port, name for
 * player 1 args[7,8,9] = host, port, name for player 2 etc...
 *
 *
 * NOTE: Because the player with tested with this server only accepted a capital case protocol some changes in the
 * Scrambler had to be made. This change influenciates games were legal plays are defined like doSomething, because it will
 * be sent as DOSOMETHING and the the unscrambling is dosomething. Please use a good player and remove this from the scrambler.
 * @author Filipe Sena
 */
public final class GDL_II_Manager {
	private GDL_II_Manager() {
	}



	public static void main(String[] args) throws IOException, SymbolFormatException, GdlFormatException,
			InterruptedException, GoalDefinitionException {
		// Extract the desired configuration from the command line.
		String tourneyName = args[0];
		String gameKey = args[1];

		// Recieves the game from here. Needs change
		Game game = GameRepository.getDefaultRepository().getGame(gameKey);

		int startClock = Integer.valueOf(args[2]);
		int playClock = Integer.valueOf(args[3]);

		System.out.println( "Imperfect-Information Game Server Started.");
		System.out.println( "Tournament Name: " + tourneyName);
		System.out.println( "Game key: " + gameKey);
		System.out.println( "Start Clock: " + startClock);
		System.out.println( "Play Clock: " + playClock);

		if ((args.length - 4) % 3 != 0) {
			System.out.println( "Invalid number of player arguments of the form host/port/name.");
			throw new RuntimeException("Invalid number of player arguments of the form host/port/name.");
		}
		List<String> hostNames = new ArrayList<String>();
		List<String> playerNames = new ArrayList<String>();
		List<Integer> portNumbers = new ArrayList<Integer>();

		List<Role> roles = Role.computeRoles(game.getRules());
		System.out.println( "Computed Roles: " + roles);

		String matchName = tourneyName + "." + gameKey + "." + System.currentTimeMillis();
		System.out.println( "MatchName: " + matchName);
		System.out.println( "Adding hosts information");
		for (int i = 4; i < args.length; i += 3) {
			String hostname = args[i];
			Integer port = Integer.valueOf(args[i + 1]);
			String name = args[i + 2];
			hostNames.add(hostname);
			portNumbers.add(port);
			playerNames.add(name);
		}


		if (hostNames.size() != roles.size()) {
			System.out.println( "Invalid number of players for game " + gameKey + ": " + hostNames.size() + " vs " + roles.size());
			throw new RuntimeException(
					"Invalid number of players for game " + gameKey + ": " + hostNames.size() + " vs " + roles.size());
		}

		System.out.println( "Creating GDL-II Match");
		II_Match match = new II_Match(matchName, -1, startClock, playClock, game, "");
		System.out.println( "Created GDL-II Match");

		match.setPlayerNamesFromHost(playerNames);
		System.out.println( "Set host information in GDL-II Match");

		// Actually run the match, using the desired configuration.
		II_GameServer server = new II_GameServer(match, hostNames, portNumbers);

		server.start();
		server.join();

		// Open up the directory for this tournament.
		// Create a "scores" file if none exists.
		File f = new File(tourneyName);
		if (!f.exists()) {
			f.mkdir();
			f = new File(tourneyName + "/scores");
			f.createNewFile();
		}

		// Open up the JSON file for this match, and save the match there.
		f = new File(tourneyName + "/" + matchName + ".json");

		if (f.exists())
			f.delete();
		BufferedWriter bw = new BufferedWriter(new FileWriter(f));
		bw.write(match.toJSON());
		bw.flush();
		bw.close();

		// Save the goals in the "/scores" file for the tournament.
		bw = new BufferedWriter(new FileWriter(tourneyName + "/scores", true));
		List<Integer> goals = server.getGoals();
		String goalStr = "";
		String playerStr = "";
		for (int i = 0; i < goals.size(); i++) {
			Integer goal = server.getGoals().get(i);
			goalStr += Integer.toString(goal);
			playerStr += playerNames.get(i);
			if (i != goals.size() - 1) {
				playerStr += ",";
				goalStr += ",";
			}
		}
		bw.write(playerStr + "=" + goalStr);
		bw.write("\n");
		bw.flush();
		bw.close();

		System.exit(0);
	}
}
