package sena.base.servers;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.ggp.base.util.game.Game;
import org.ggp.base.util.game.GameRepository;
import org.ggp.base.util.gdl.factory.exceptions.GdlFormatException;
import org.ggp.base.util.statemachine.Role;
import org.ggp.base.util.statemachine.exceptions.GoalDefinitionException;
import org.ggp.base.util.symbol.factory.exceptions.SymbolFormatException;

import sena.base.server.III_GameServer;
import sena.base.util.match.II_Match;

/**
 * Extension of a java class GameServerRunner of the authors:
 * @author Evan Cox
 * @author Sam Schreiber
 *
 * GDL_III_Manager is a utility program that lets you start up a match directly
 * from the command line. It runs an instance of a match, and finishes it.
 *
 * It takes the following arguments: args[0] = tournament name, for storing
 * results args[1] = game key, for loading the game args[2] = start clock, in
 * seconds args[3] = play clock, in seconds args[4,5,6] = host, port, name for
 * player 1 args[7,8,9] = host, port, name for player 2 etc...
 *
 * @author Filipe Sena
 */
public final class GDL_III_Manager {
	private GDL_III_Manager() {
	}

	//private static final Logger LOGGER = Logger.getLogger( GDL_III_Manager.class.getName() );


	public static void main(String[] args) throws IOException, SymbolFormatException, GdlFormatException,
			InterruptedException, GoalDefinitionException {

		if(args.length < 9) {
			System.err.println( "Usage: GDL_III_Manager "
					+ "[TournamentName] [GameKey] [StartClock] [PlayClock] [TotalNrModels] [NInconsistentModels] "
					+ "{For each role in Game Description: [PlayerAddress] [playerPort] [PlayerName] ");
			System.err.println( "E.g: myTournament muddyChildren3 60 15 100 90 127.0.0.1 9999 RandomPlayer 127.0.0.1 4001 PlayerOne "
					+ "127.0.0.1 4002 PlayerTwo 127.0.0.1 4003 PlayerThree");
			throw new RuntimeException("Invalid number of arguments");
		}

		// Extract the desired configuration from the command line.
		String tourneyName = args[0];
		String gameKey = args[1];

		// Recieves the game from here. Needs change
		Game game = GameRepository.getDefaultRepository().getGame(gameKey);

		int startClock = Integer.valueOf(args[2]);
		int playClock = Integer.valueOf(args[3]);

    int nrModels = Integer.valueOf(args[4]);
		int inconsistent = Integer.valueOf(args[5]);

		System.out.println("Epistemic Game Server Started.");

		System.out.println( "Tournament Name: " + tourneyName);
		System.out.println( "Game key: " + gameKey);
		System.out.println( "Start Clock: "+ startClock);
		System.out.println( "Play Clock: " + playClock);
		System.out.println( "Number of models for model sampling: "+ nrModels);
		System.out.println( "Number of inconsistent models allowed: " + inconsistent);

		if ((args.length - 6) % 3 != 0) {
			System.out.println( "Invalid number of player arguments of the form host/port/name.");
			throw new RuntimeException("Invalid number of player arguments of the form host/port/name.");
		}
		List<String> hostNames = new ArrayList<String>();
		List<String> playerNames = new ArrayList<String>();
		List<Integer> portNumbers = new ArrayList<Integer>();

		List<Role> roles = Role.computeRoles(game.getRules());
		System.out.println( "Computed Roles: " + roles);

		String matchName = tourneyName + "." + gameKey + "." + System.currentTimeMillis();
		System.out.println( "MatchName: " + matchName);
		System.out.println("Adding hosts information");

		for (int i = 6; i < args.length; i += 3) {
			String hostname = args[i];
			Integer port = Integer.valueOf(args[i + 1]);
			String name = args[i + 2];
			hostNames.add(hostname);
			System.out.println( "Added host address: " + hostname);
			portNumbers.add(port);
			System.out.println( "Added host port: " + port);
			playerNames.add(name);
			System.out.println( "Added name: " + name);
		}

		if (hostNames.size() != roles.size()) {
			System.out.println( "Invalid number of players for game " + gameKey + ": " + hostNames.size() + " vs " + roles.size());
			throw new RuntimeException(
					"Invalid number of players for game " + gameKey + ": " + hostNames.size() + " vs " + roles.size());
		}

		System.out.println( "Creating GDL-III Match");
		II_Match match = new II_Match(matchName, -1, startClock, playClock, game, "");

		match.setPlayerNamesFromHost(playerNames);

		// Actually run the match, using the desired configuration.
		System.out.println( "Creating GDL-III Server thread");
		III_GameServer server = new III_GameServer(match, nrModels, inconsistent, hostNames, portNumbers);

		System.out.println( "Starting Game Server Thread");
		server.start();
		server.join();
		System.out.println( "Joined thread Game Server thread");

		// Open up the directory for this tournament.
		// Create a "scores" file if none exists.
		System.out.println( "Creating directory " + tourneyName +" to save results");
		File f = new File(tourneyName);
		if (!f.exists()) {
			f.mkdir();
			f = new File(tourneyName + "/scores");
			f.createNewFile();
		}
		System.out.println( "Created directory" );

		// Open up the JSON file for this match, and save the match there.
		f = new File(tourneyName + "/" + matchName + ".json");

		System.out.println("Writing match information to file: " + f.getName());
		if (f.exists())
			f.delete();
		BufferedWriter bw = new BufferedWriter(new FileWriter(f));

		bw.write(match.toJSON());
		bw.flush();
		bw.close();

		// Save the goals in the "/scores" file for the tournament.
		System.out.println( "Writing game scores in: " + tourneyName + "/scores");
		bw = new BufferedWriter(new FileWriter(tourneyName + "/scores", true));
		List<Integer> goals = server.getGoals();
		String goalStr = "";
		String playerStr = "";
		for (int i = 0; i < goals.size(); i++) {
			Integer goal = server.getGoals().get(i);
			goalStr += Integer.toString(goal);
			playerStr += playerNames.get(i);
			if (i != goals.size() - 1) {
				playerStr += ",";
				goalStr += ",";
			}
		}
		bw.write(playerStr + "=" + goalStr);
		bw.write("\n");
		bw.flush();
		bw.close();

		System.exit(0);
	}
}
