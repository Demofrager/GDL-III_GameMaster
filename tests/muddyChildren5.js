#!/usr/bin/env node
const {execSync} = require('child_process')

// print process.argv
let models = process.argv[2] // first argument
let iModels = process.argv[3] // second argument
let tests = process.argv[4] //third argument

let cmdString = "cd ..; ant GDL_III_Manager_5_Players -Darg0=myTournament -Darg1=muddyChildren5 -Darg2=60 -Darg3=15 -Darg4=" + models + " -Darg5=" + iModels + " -Darg6=127.0.0.1 -Darg7=9999 -Darg8=RandomPlayer -Darg9=127.0.0.1 -Darg10=4001 -Darg11=PlayerOne -Darg12=127.0.0.1 -Darg13=4002 -Darg14=PlayerTwo -Darg15=127.0.0.1 -Darg16=4003 -Darg17=PlayerThree -Darg18=127.0.0.1 -Darg19=4004 -Darg20=PlayerFour -Darg21=127.0.0.1 -Darg22=4005 -Darg23=PlayerFive"

let print = ''
let totalTime = 0
let passed = 0
let avgTime = 0

for(let i = 0; i < tests; i++){
  let cmd = execSync(cmdString).toString()
  
  let [,n1,n2,n3,n4,n5] = cmd.match(/\( muddy (\d) (\d) (\d) (\d) (\d) \)/)
  let sum = +n1 + +n2 + +n3 + +n4 + +n5 + 1
  let [,total] = cmd.match(/Sending stop requests with turn: (\d)/)
  let [,time1] = cmd.match(/Total: (\d+)/)
  let [,time2] = cmd.match(/Avg: (\d+)/)
  
  if(sum > +total) {
    console.log("(" + i + ") Test (" + n1 + " " + n2 + " " + n3 + " " + n4 + " " + n5 +  "): partial failure")
    console.log("   Expected: " + sum)
    console.log("   Reality: " + total)
  }

  else if(sum == +total){
    console.log("(" + i + ") Test (" + n1 + " " + n2 + " " + n3 + " " + n4 + " " + n5 +  "): pass")
    passed++
    totalTime += +time1
    avgTime += +time2
  }
  else {
    console.log("(" + i + ") Test (" + n1 + " " + n2 + " " + n3 + " " + n4 + " " + n5 +  "): complete failure")
    console.log("   Expected: " + sum)
    console.log("   Reality: " + total)
  }
}
console.log()
console.log('Statistics:\n  TotalTime(ms): ' + totalTime/passed + '\n  AvgTime(ms): ' + avgTime/passed)

